<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.mybox.charge.show', function ($trail) {
    $trail->push('شحن الصندوق', route('admin.mybox.charge.show'));
});

Breadcrumbs::for('admin.backup.db', function ($trail) {
    $trail->push('النسخ الاحتياطي', route('admin.backup.db'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
require __DIR__.'/blogs/blog.php';
require __DIR__.'/blog-categories/blog-categories.php';
require __DIR__.'/blog-tags/blog-tags.php';
require __DIR__.'/pages/page.php';
require __DIR__.'/faqs/faq.php';
require __DIR__.'/email-templates/email-template.php';
require __DIR__.'/auth/permission.php';
require __DIR__.'/Companys/company.php';
require __DIR__.'/Clients/client.php';
require __DIR__.'/Transfers/transfer.php';
require __DIR__.'/Costs/cost.php';
