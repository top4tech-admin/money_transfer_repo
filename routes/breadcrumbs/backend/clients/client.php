<?php
Breadcrumbs::for('admin.clients.index', function ($trail) {
    $trail->push('ادارة الزبائن', route('admin.clients.index'));
});

Breadcrumbs::for('admin.clients.create', function ($trail) {
    $trail->parent('admin.clients.index');
    $trail->push('انشاء زبون جديد', route('admin.clients.create'));
});

Breadcrumbs::for('admin.clients.edit', function ($trail, $id) {
    $trail->parent('admin.clients.index');
    $trail->push('تعديل معلومات زبون', route('admin.clients.edit', $id));
});
