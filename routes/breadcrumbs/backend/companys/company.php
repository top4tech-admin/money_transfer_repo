<?php
Breadcrumbs::for('admin.companys.index', function ($trail) {
    $trail->push('ادارة companys', route('admin.companys.index'));
});

Breadcrumbs::for('admin.companys.create', function ($trail) {
    $trail->parent('admin.companys.index');
    $trail->push('ادارة companys', route('admin.companys.create'));
});

Breadcrumbs::for('admin.companys.edit', function ($trail, $id) {
    $trail->parent('admin.companys.index');
    $trail->push('ادارة companys', route('admin.companys.edit', $id));
});

Breadcrumbs::for('admin.companys.show', function ($trail, $id) {
    $trail->parent('admin.auth.user.index');
    $trail->push('ادارة companys', route('admin.companys.show', $id));
});

Breadcrumbs::for('admin.companys.charge.show', function ($trail, $company) {
    $trail->push('شحن رصيد', route('admin.companys.charge.show', $company));
});

Breadcrumbs::for('admin.companys.charge.log', function ($trail, $company) {
    $trail->push('سجل الشحن', route('admin.companys.charge.log', $company));
});