<?php
Breadcrumbs::for('admin.transfers.index', function ($trail) {
    $trail->push('ادارة الحوالات', route('admin.transfers.index'));
});

Breadcrumbs::for('admin.transfers.create', function ($trail) {
    $trail->parent('admin.transfers.index');
    $trail->push('انشاء حوالة', route('admin.transfers.create'));
});

Breadcrumbs::for('admin.transfers.edit', function ($trail, $id) {
    $trail->parent('admin.transfers.index');
    $trail->push('تعديل الحوالة', route('admin.transfers.edit', $id));
});

Breadcrumbs::for('admin.transfers.recive', function ($trail) {
    $trail->parent('admin.transfers.index');
    $trail->push('تسليم الحوالة', route('admin.transfers.recive'));
});

Breadcrumbs::for('admin.transfers.recived', function ($trail) {
    $trail->parent('admin.transfers.index');
    $trail->push('الحوالات المسلمة', route('admin.transfers.recived'));
});

Breadcrumbs::for('admin.transfers.unrecived', function ($trail) {
    $trail->parent('admin.transfers.index');
    $trail->push('الحوالات غير المسلمة', route('admin.transfers.unrecived'));
});

Breadcrumbs::for('admin.transfers.company', function ($trail, $com_id) {
    $trail->push('حوالات شركة', route('admin.transfers.company', $com_id));
});

Breadcrumbs::for('admin.transfers.show', function ($trail, $transfer) {
    $trail->push('تفاصيل الحوالة', route('admin.transfers.show', $transfer));
});
