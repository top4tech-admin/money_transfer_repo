<?php

// Clients Management
Route::group(['namespace' => 'Clients'], function () {
    Route::resource('clients', 'ClientsController', ['except' => ['show']]);

    //For DataTables
    Route::post('clients/get', 'ClientsTableController')->name('clients.get');
});