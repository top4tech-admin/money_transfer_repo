<?php

	// Transfers Management
	Route::group(['namespace' => 'Transfers'], function () {
    
	
    //For DataTables Recived & Unrecived
    Route::post('transfers/get', 'TransfersTableController')->name('transfers.get');
	
	//--------------------------------------
	//For DataTables Recived Transfers
    Route::post('transfers/recived/get', 'TransfersTableController@recivedTransfers')->name('transfers.recived.get');
	// blade page
	Route::get('transfers/recived', 'TransfersController@recivedTransfers')->name('transfers.recived');
	
	//For DataTables Unrecived Transfers
    Route::post('transfers/unrecived/get', 'TransfersTableController@unRecivedTransfers')->name('transfers.unrecived.get');
	// blade page
	Route::get('transfers/unrecived', 'TransfersController@unRecivedTransfers')->name('transfers.unrecived');
	
	//For DataTables Transfers For Specific Company 
    Route::post('transfers/company/get/{com_id}', 'TransfersTableController@getTransferOfCompany')->name('transfers.company.get');
	// blade page
	Route::get('transfers/company/{com_id}', 'TransfersController@companyTransfers')->name('transfers.company');
	//--------------------------------------
	
	//Transfer blade page
	Route::get('transfers/recive', 'TransfersController@reciveTransfer')->name('transfers.recive');
	
	//Recive Operation 
	Route::PATCH('transfers/recive/{transfer}', 'TransfersController@recive')->name('transfers.recive.patch');
	
	Route::get('transfers/rollback/{transfer}', 'TransfersController@rollbackTransfer')->name('transfers.rollback.get');
	
	Route::resource('transfers', 'TransfersController');
});