<?php
use App\Http\Controllers\Backend\Companys\CompanysController;

// Companys Management
Route::group(['namespace' => 'Companys'], function () {
    Route::resource('companys', 'CompanysController', ['except' => ['show']]);

	Route::get('companys/{company}', [CompanysController::class, 'show'])->name('companys.show');

    //For DataTables
    Route::post('companys/get', 'CompanysTableController')->name('companys.get');
	
	// charge page
	Route::get('companys/charge/{company}', [CompanysController::class, 'chargeBalance'])->name('companys.charge.show');
	
	// charge Company operation
	Route::post('companys/charge/{company}', [CompanysController::class, 'charge'])->name('companys.charge');
	
	// charge Main Box operation
	Route::post('mybox/charge/{mybox}', [CompanysController::class, 'chargeBox'])->name('mybox.charge');
	
	// company charge log table 
	Route::post('companys/charge/log/{company_id}', 'CompanysTableController@getChargeLogTable')->name('companys.charge.log.get');
	// company charge blade table 
	Route::get('companys/charge/log/{company_id}', [CompanysController::class, 'chargeLogView'])->name('companys.charge.log');
	
	Route::PATCH('companys/charge/update/{charge}', [CompanysController::class, 'updateCharge'])->name('companys.charges.update');
	
	Route::DELETE('companys/charge/destroy/{charge}', [CompanysController::class, 'destroyCharge'])->name('companys.charges.destroy');
});