<?php

    namespace App\Http\Resources;
    
    use Illuminate\Http\Resources\Json\Resource;
    
    class ClientsResource extends Resource
    {
        /**
         * Transform the resource into an array.
         *
         * @param  \Illuminate\Http\Request
         *
         * @return array
         */
        public function toArray($request)
        {
            return [
				'id'=>$this->id,
                'name'=>$this->name,
				'identification'=>$this->identification,
				'finger_print'=>$this->finger_print,
				'mother_name'=>$this->mother_name,
				'birthday'=>$this->birthday,
				'phone'=>$this->phone
            ];
        }
    }
    
    