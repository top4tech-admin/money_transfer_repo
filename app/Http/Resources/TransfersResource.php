<?php

    namespace App\Http\Resources;
    
    use Illuminate\Http\Resources\Json\Resource;
    
    class TransfersResource extends Resource
    {
        /**
         * Transform the resource into an array.
         *
         * @param  \Illuminate\Http\Request
         *
         * @return array
         */
        public function toArray($request)
        {
            return [
				'id'=>$this->id,
                'company'=>$this->company->name,
				'client'=>$this->client->name,
				'amount'=>$this->amount,
				'note'=>$this->note,
				'mother_name'=>$this->client->mother_name,
				'birthday'=>$this->client->birthday,
				'created_at'=>$this->created_at->format('j/m/Y - h:i A'),
				'status'=>$this->status
            ];
        }
    }
    
    