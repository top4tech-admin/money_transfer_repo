<?php
    namespace App\Http\Requests\Backend\Clients;
    
    use Illuminate\Foundation\Http\FormRequest;
    
    class StoreClientRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return access()->allow('edit-page');
        }
    
        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                // nullable : allow empty and null value.
				'phone' => ['nullable','unique:clients,phone'],
            ];
        }
    }
    