<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Backend\Clients\DeleteClientRequest;
use App\Http\Requests\Backend\Clients\ManageClientRequest;
use App\Http\Requests\Backend\Api\ApiRequest;
use App\Http\Requests\Backend\Clients\StoreClientRequest;
use App\Http\Requests\Backend\Clients\UpdateClientRequest;
use App\Http\Resources\ClientsResource;
use App\Models\Client;
use App\Repositories\Backend\ClientsRepository;
use Illuminate\Http\Response;

/**
 * @group Clients Management
 *
 * Class ClientsController
 *
 * APIs for Clients Management
 *
 * @authenticated
 */
class ClientsController extends APIController
{
    /**
     * Repository.
     *
     * @var ClientsRepository
     */
    protected $repository;

    /**
     * __construct.
     *
     * @param $repository
     */
    public function __construct(ClientsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get all Clients.
     *
     * This endpoint provides a paginated list of all clients. You can customize how many records you want in each
     * returned response as well as sort records based on a key in specific order.
     *
     * @queryParam client Which client to show. Example: 12
     * @queryParam per_client Number of records per client. (use -1 to retrieve all) Example: 20
     * @queryParam order_by Order by database column. Example: created_at
     * @queryParam order Order direction ascending (asc) or descending (desc). Example: asc
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/client/client-list.json
     *
     * @param \Illuminate\Http\ManageClientRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ManageClientRequest $request)
    {
        $collection = $this->repository->retrieveList($request->all());

        return ClientsResource::collection($collection);
    }

    // get client according to the category
    public function getClientbyId(ApiRequest $request, int $id)
    {
        return new ClientsResource(Client::find($id));
    }

    // get client according to the category
    public function searchForClient(ApiRequest $request, String $client_name)
    {
        $collection = $this->repository->RetrieveSearchList($request->all(), $client_name);

        return ClientsResource::collection($collection);
    }
	
    /**
     * Gives a specific Client.
     *
     * This endpoint provides you a single Client
     * The Client is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Client
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/client/client-show.json
     *
     * @param ManageClientRequest $request
     * @param \App\Models\Client $client
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ManageClientRequest $request, Client $client)
    {
        return new ClientsResource($client);
    }

    /**
     * Create a new Client.
     *
     * This endpoint lets you create new Client
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile status=201 responses/client/client-store.json
     *
     * @param \App\Http\Requests\Backend\Clients\StoreClientRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreClientRequest $request)
    {
        $client = $this->repository->create($request->validated());

        return (new ClientsResource($client))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Update Client.
     *
     * This endpoint allows you to update existing Client with new data.
     * The Client to be updated is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Client
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/client/client-update.json
     *
     * @param \App\Models\Client $client
     * @param \App\Http\Requests\Backend\Clients\UpdateClientRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateClientRequest $request, Client $client)
    {
        $client = $this->repository->update($client, $request->validated());

        return new ClientsResource($client);
    }

    /**
     * Delete Client.
     *
     * This endpoint allows you to delete a Client
     * The Client to be deleted is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Client
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile status=204 scenario="When the record is deleted" responses/client/client-destroy.json
     *
     * @param \App\Models\Client $client
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteClientRequest $request, Client $client)
    {
        $this->repository->delete($client);

        return response()->noContent();
    }
}
