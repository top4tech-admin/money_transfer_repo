<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Backend\Companys\DeleteCompanyRequest;
use App\Http\Requests\Backend\Companys\ManageCompanyRequest;
use App\Http\Requests\Backend\Companys\StoreCompanyRequest;
use App\Http\Requests\Backend\Companys\UpdateCompanyRequest;
use App\Http\Resources\CompanysResource;
use App\Models\Company;
use App\Repositories\Backend\CompanysRepository;
use Illuminate\Http\Response;

/**
 * @group Companys Management
 *
 * Class CompanysController
 *
 * APIs for Companys Management
 *
 * @authenticated
 */
class CompanysController extends APIController
{
    /**
     * Repository.
     *
     * @var CompanysRepository
     */
    protected $repository;

    /**
     * __construct.
     *
     * @param $repository
     */
    public function __construct(CompanysRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get all Companys.
     *
     * This endpoint provides a paginated list of all companys. You can customize how many records you want in each
     * returned response as well as sort records based on a key in specific order.
     *
     * @queryParam company Which company to show. Example: 12
     * @queryParam per_company Number of records per company. (use -1 to retrieve all) Example: 20
     * @queryParam order_by Order by database column. Example: created_at
     * @queryParam order Order direction ascending (asc) or descending (desc). Example: asc
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/company/company-list.json
     *
     * @param \Illuminate\Http\ManageCompanyRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ManageCompanyRequest $request)
    {
        $collection = $this->repository->retrieveList($request->all());

        return CompanysResource::collection($collection);
    }

    /**
     * Gives a specific Company.
     *
     * This endpoint provides you a single Company
     * The Company is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Company
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/company/company-show.json
     *
     * @param ManageCompanyRequest $request
     * @param \App\Models\Company $company
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ManageCompanyRequest $request, Company $company)
    {
        return new CompanysResource($company);
    }

    /**
     * Create a new Company.
     *
     * This endpoint lets you create new Company
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile status=201 responses/company/company-store.json
     *
     * @param \App\Http\Requests\Backend\Companys\StoreCompanyRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCompanyRequest $request)
    {
        $company = $this->repository->create($request->validated());

        return (new CompanysResource($company))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Update Company.
     *
     * This endpoint allows you to update existing Company with new data.
     * The Company to be updated is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Company
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/company/company-update.json
     *
     * @param \App\Models\Company $company
     * @param \App\Http\Requests\Backend\Companys\UpdateCompanyRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateCompanyRequest $request, Company $company)
    {
        $company = $this->repository->update($company, $request->validated());

        return new CompanysResource($company);
    }

    /**
     * Delete Company.
     *
     * This endpoint allows you to delete a Company
     * The Company to be deleted is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Company
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile status=204 scenario="When the record is deleted" responses/company/company-destroy.json
     *
     * @param \App\Models\Company $company
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteCompanyRequest $request, Company $company)
    {
        $this->repository->delete($company);

        return response()->noContent();
    }
}
