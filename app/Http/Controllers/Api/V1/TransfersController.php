<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Backend\Transfers\DeleteTransferRequest;
use App\Http\Requests\Backend\Transfers\ManageTransferRequest;
use App\Http\Requests\Backend\Api\ApiRequest;
use App\Http\Requests\Backend\Transfers\StoreTransferRequest;
use App\Http\Requests\Backend\Transfers\UpdateTransferRequest;
use App\Http\Resources\TransfersResource;
use App\Models\Transfer;
use App\Repositories\Backend\TransfersRepository;
use Illuminate\Http\Response;

/**
 * @group Transfers Management
 *
 * Class TransfersController
 *
 * APIs for Transfers Management
 *
 * @authenticated
 */
class TransfersController extends APIController
{
    /**
     * Repository.
     *
     * @var TransfersRepository
     */
    protected $repository;

    /**
     * __construct.
     *
     * @param $repository
     */
    public function __construct(TransfersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get all Transfers.
     *
     * This endpoint provides a paginated list of all transfers. You can customize how many records you want in each
     * returned response as well as sort records based on a key in specific order.
     *
     * @queryParam transfer Which transfer to show. Example: 12
     * @queryParam per_transfer Number of records per transfer. (use -1 to retrieve all) Example: 20
     * @queryParam order_by Order by database column. Example: created_at
     * @queryParam order Order direction ascending (asc) or descending (desc). Example: asc
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/transfer/transfer-list.json
     *
     * @param \Illuminate\Http\ManageTransferRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ManageTransferRequest $request)
    {
        $collection = $this->repository->retrieveList($request->all());

        return TransfersResource::collection($collection);
    }

    /**
     * Gives a specific Transfer.
     *
     * This endpoint provides you a single Transfer
     * The Transfer is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Transfer
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/transfer/transfer-show.json
     *
     * @param ManageTransferRequest $request
     * @param \App\Models\Transfer $transfer
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ManageTransferRequest $request, Transfer $transfer)
    {
        return new TransfersResource($transfer);
    }

    /**
     * Create a new Transfer.
     *
     * This endpoint lets you create new Transfer
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile status=201 responses/transfer/transfer-store.json
     *
     * @param \App\Http\Requests\Backend\Transfers\StoreTransferRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreTransferRequest $request)
    {
        $transfer = $this->repository->create($request->validated());

        return (new TransfersResource($transfer))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Update Transfer.
     *
     * This endpoint allows you to update existing Transfer with new data.
     * The Transfer to be updated is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Transfer
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/transfer/transfer-update.json
     *
     * @param \App\Models\Transfer $transfer
     * @param \App\Http\Requests\Backend\Transfers\UpdateTransferRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateTransferRequest $request, Transfer $transfer)
    {
        $transfer = $this->repository->update($transfer, $request->validated());

        return new TransfersResource($transfer);
    }

    /**
     * Delete Transfer.
     *
     * This endpoint allows you to delete a Transfer
     * The Transfer to be deleted is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Transfer
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile status=204 scenario="When the record is deleted" responses/transfer/transfer-destroy.json
     *
     * @param \App\Models\Transfer $transfer
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteTransferRequest $request, Transfer $transfer)
    {
        $this->repository->delete($transfer);

        return response()->noContent();
    }
	
	public function getTransfersNotRecivedByClientId(ApiRequest $request, int $id)
	{
		$collection = $this->repository->retriveTransfersList($request->all(), $id);
		
		return TransfersResource::collection($collection);
	}
}
