<?php

namespace App\Http\Controllers\Backend\Clients;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Clients\CreateClientRequest;
use App\Http\Requests\Backend\Clients\DeleteClientRequest;
use App\Http\Requests\Backend\Clients\EditClientRequest;
use App\Http\Requests\Backend\Clients\ManageClientRequest;
use App\Http\Requests\Backend\Clients\StoreClientRequest;
use App\Http\Requests\Backend\Clients\UpdateClientRequest;
use App\Http\Responses\Backend\Client\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Client;
use App\Repositories\Backend\ClientsRepository;
use Illuminate\Support\Facades\View;

class ClientsController extends Controller
{
    /**
     * @var \App\Repositories\Backend\ClientsRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\ClientsRepository $repository
     */
    public function __construct(ClientsRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['clients']);
    }

    /**
     * @param \App\Http\Requests\Backend\Clients\ManageClientRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageClientRequest $request)
    {
        return new ViewResponse('backend.clients.index');
    }

    /**
     * @param \App\Http\Requests\Backend\Clients\CreateClientRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreateClientRequest $request)
    {
        return new ViewResponse('backend.clients.create');
    }

    /**
     * @param \App\Http\Requests\Backend\Clients\StoreClientRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreClientRequest $request)
    {
        $this->repository->create($request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.clients.index'), ['flash_success' => 'تم انشاء client بنجاح']);
    }

    /**
     * @param \App\Models\Client $client
     * @param \App\Http\Requests\Backend\Clients\EditClientRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(Client $client, EditClientRequest $request)
    {
        return new EditResponse($client);
    }

    /**
     * @param \App\Models\Client $client
     * @param \App\Http\Requests\Backend\Clients\UpdateClientRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Client $client, UpdateClientRequest $request)
    {
        $this->repository->update($client, $request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.clients.index'), ['flash_success' => 'تم تحديث client بنجاح']);
    }

    /**
     * @param \App\Models\Client $client
     * @param \App\Http\Requests\Backend\Clients\DeleteClientRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Client $client, DeleteClientRequest $request)
    {
        $this->repository->delete($client);

        return new RedirectResponse(route('admin.clients.index'), ['flash_success' => 'تم حذف client بنجاح']);
    }
}

