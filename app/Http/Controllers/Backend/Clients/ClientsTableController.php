<?php

namespace App\Http\Controllers\Backend\Clients;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Clients\ManageClientRequest;
use App\Repositories\Backend\ClientsRepository;
use Yajra\DataTables\Facades\DataTables;

class ClientsTableController extends Controller
{
    /**
     * @var \App\Repositories\Backend\ClientsRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\ClientsRepository $repository
     */
    public function __construct(ClientsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \App\Http\Requests\Backend\Clients\Manage\ClientRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageClientRequest $request)
    {
        return Datatables::of($this->repository->getForDataTable())
            ->editColumn('created_at', function ($client) {
                return $client->created_at->toDateString();
            })
            ->addColumn('actions', function ($client) {
                return $client->action_buttons;
            })
            ->escapeColumns(['title'])
            ->make(true);
    }
}
