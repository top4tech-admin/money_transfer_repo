<?php

namespace App\Http\Controllers\Backend\Transfers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Transfers\CreateTransferRequest;
use App\Http\Requests\Backend\Transfers\DeleteTransferRequest;
use App\Http\Requests\Backend\Transfers\EditTransferRequest;
use App\Http\Requests\Backend\Transfers\ManageTransferRequest;
use App\Http\Requests\Backend\Transfers\StoreTransferRequest;
use App\Http\Requests\Backend\Transfers\UpdateTransferRequest;
use App\Http\Responses\Backend\Transfer\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Transfer;
use App\Models\Company;
use App\Models\Client;
use App\Repositories\Backend\ClientsRepository;
use App\Repositories\Backend\TransfersRepository;
use Illuminate\Support\Facades\View;

class TransfersController extends Controller
{
    /**
     * @var \App\Repositories\Backend\TransfersRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\TransfersRepository $repository
     */
    public function __construct(TransfersRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['transfers']);
    }

    /**
     * @param \App\Http\Requests\Backend\Transfers\ManageTransferRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageTransferRequest $request)
    {
        
        $companies = Company::get(['id','name']);
        return new ViewResponse('backend.transfers.index',["companies" => $companies]);
    }

    /**
     * @param \App\Http\Requests\Backend\Transfers\CreateTransferRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreateTransferRequest $request)
    {
		$companies = Company::get(['id','name']);
        return new ViewResponse('backend.transfers.create',["companies" => $companies]);
    }

    /**
     * @param \App\Http\Requests\Backend\Transfers\StoreTransferRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreTransferRequest $request)
    {
		$request['created_by'] = auth()->user()->id;
		
		//  اذا لم يكن الزبون موجود مسبقا بقاعدة البيانات سنقوم بانشاءه
		if(empty($request['client_id'])){
			$client = new ClientsRepository();
			$client_id = $client->create(['name'=>$request['client-name'],'phone'=>$request['client-phone']]);
			$request['client_id'] = $client_id->id;
		}
		
		$this->repository->create($request->except(['_token', '_method']));
        
		return new RedirectResponse(route('admin.transfers.create'), ['flash_success' => 'تم انشاء حوالة بنجاح','company_id' => $request['company_id']]);
    }

    /**,
     * @param \App\Models\Transfer $transfer
     * @param \App\Http\Requests\Backend\Transfers\EditTransferRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(Transfer $transfer, EditTransferRequest $request)
    {
        return new EditResponse($transfer);
    }

    /**
     * @param \App\Models\Transfer $transfer
     * @param \App\Http\Requests\Backend\Transfers\UpdateTransferRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Transfer $transfer, UpdateTransferRequest $request)
    {
        $this->repository->update($transfer, $request->except(['_token', '_method']));

        return new RedirectResponse($request->header('referer'), ['flash_success' => 'تمت العملية بنجاح']);
    }
	
	//---------------
	public function recive(Transfer $transfer, UpdateTransferRequest $request) 
    {
		$request['delivered_by'] = auth()->user()->id;
		$request['status'] = "T";
		$request['delivered_at'] = date("Y-m-d H:i:s");
			
        $transfer = $this->repository->reciveTransfer($transfer, $request->except(['_token', '_method']));
		
		$client = Client::find($transfer->client_id);
		$client_repo = new ClientsRepository();
		$client_id = $client_repo->update($client, ['mother_name'=>$request['mother_name'],'birthday'=>$request['birthday']]);

		return redirect()->back()->with(['flash_success' => ' تم تسليم الحوالة بنجاح ']); 
        //return new RedirectResponse(route('admin.transfers.recive'), ['flash_success' => ' تم تسليم الحوالة بنجاح ']);
    }
	//--------------------
	public function rollbackTransfer(Transfer $transfer, UpdateTransferRequest $request)
	{
		$this->repository->rollbackTransfer($transfer, $request->except(['_token', '_method']));

        return new RedirectResponse($request->header('referer'), ['flash_success' => 'تمت العملية بنجاح']);
	}
	//--------------------
    /**
     * @param \App\Models\Transfer $transfer
     * @param \App\Http\Requests\Backend\Transfers\DeleteTransferRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Transfer $transfer, DeleteTransferRequest $request)
    {
        $this->repository->delete($transfer);

        return new RedirectResponse(route('admin.transfers.index'), ['flash_success' => 'تم حذف الحوالة بنجاح']);
    }
	
	/**
     * @param \App\Models\Transfer $transfer
     * @param \App\Http\Requests\Backend\Transfers\UpdateTransferRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function reciveTransfer(ManageTransferRequest $request)
    {
        return new ViewResponse('backend.transfers.recive');
    }
	
	public function recivedTransfers(ManageTransferRequest $request)
    {
        $companies = Company::get(['id','name']);
        return new ViewResponse('backend.transfers.recived-table',["companies" => $companies]);
    }
	
	public function unRecivedTransfers(ManageTransferRequest $request)
    {
        return new ViewResponse('backend.transfers.unrecived-table');
    }
	
	public function companyTransfers(ManageTransferRequest $request, $com_id)
	{
		return new ViewResponse('backend.transfers.com-transfer-table',['com_id' => $com_id]);
	}
	
	public function show(ManageTransferRequest $request, Transfer $transfer)
    {
        return new ViewResponse('backend.transfers.show',['transfer'=>$transfer]);
    }
}

