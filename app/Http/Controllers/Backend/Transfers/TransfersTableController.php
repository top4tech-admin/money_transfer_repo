<?php

namespace App\Http\Controllers\Backend\Transfers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Transfers\ManageTransferRequest;
use App\Repositories\Backend\TransfersRepository;
use Yajra\DataTables\Facades\DataTables;

class TransfersTableController extends Controller
{
    /**
     * @var \App\Repositories\Backend\TransfersRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\TransfersRepository $repository
     */
    public function __construct(TransfersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \App\Http\Requests\Backend\Transfers\Manage\TransferRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageTransferRequest $request)
    {
        return Datatables::of($this->repository->getForDataTable($request['date'], $request['companies']))
			->editColumn('company_id',function($transfer)
			{
				return $transfer->company->name;
			})
			->editColumn('status',function($transfer)
			{
				return $transfer->status == 'F' ? "غير مسلمة" : "تم تسليمها";
			})
            ->addColumn('actions', function ($transfer) {
				if($transfer->status == 'F'){
					return $transfer->action_buttons;
				}else{
					if(auth()->user()->isAdmin()){
						return ($transfer->view_button . $transfer->rollback_button);
					}else{
						return $transfer->view_button;
					}
				}
            })
            ->escapeColumns(['title'])
            ->make(true);
    }
	
	public function recivedTransfers(ManageTransferRequest $request)
	{
		return Datatables::of($this->repository->getTransferStatusT($request['date'], $request['companies']))
		->editColumn('company_id',function($transfer)
		{
			return $transfer->company->name;
		})
		->editColumn('status',function($transfer)
		{
			return $transfer->status == 'F' ? "غير مسلمة" : "تم تسليمها";
		})
		->addColumn('actions', function ($transfer) {
                if($transfer->status == 'F'){
					return $transfer->action_buttons;
				}else{
					if(auth()->user()->isAdmin()){
						return ($transfer->view_button . $transfer->rollback_button);
					}else{
						return $transfer->view_button;
					}
				}
            })
		->escapeColumns(['title'])
		->make(true);
	}
	
	public function unRecivedTransfers(ManageTransferRequest $request)
	{
		return Datatables::of($this->repository->getTransferStatusF($request['date']))
		->editColumn('company_id',function($transfer)
		{			
			return $transfer->company->name;
		})
		->editColumn('status',function($transfer)
		{
			return $transfer->status == 'F' ? "غير مسلمة" : "تم تسليمها";
		})
		->addColumn('actions', function ($transfer) {
                if($transfer->status == 'F'){
					return $transfer->action_buttons;
				}else{
					return $transfer->view_button;
				}
            })
		->escapeColumns(['title'])
		->make(true);
	}
	
	public function getTransferOfCompany(ManageTransferRequest $request,  $com_id)
	{
		return Datatables::of($this->repository->getTransferforCompany($com_id))
		->editColumn('company_id',function($transfer)
		{
			return $transfer->company->name;
		})
		->editColumn('status',function($transfer)
		{
			return $transfer->status == 'F' ? "غير مسلمة" : "تم تسليمها";
		})
		->addColumn('actions', function ($transfer) {
                if($transfer->status == 'F'){
					return $transfer->action_buttons;
				}else{
					return $transfer->view_button;
				}
            })
		->escapeColumns(['title'])
		->make(true);
	}
}
