<?php

namespace App\Http\Controllers\Backend\Companys;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Companys\ManageCompanyRequest;
use App\Repositories\Backend\CompanysRepository;
use Yajra\DataTables\Facades\DataTables;

class CompanysTableController extends Controller
{
    /**
     * @var \App\Repositories\Backend\CompanysRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\CompanysRepository $repository
     */
    public function __construct(CompanysRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \App\Http\Requests\Backend\Companys\Manage\CompanyRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageCompanyRequest $request)
    {
        return Datatables::of($this->repository->getForDataTable())
            ->editColumn('created_at', function ($company) {
                return $company->created_at->toDateString();
            })
            ->addColumn('actions', function ($company) {
                return $company->action_buttons;
            })
            ->escapeColumns(['title'])
            ->make(true);
    }
	
	public function getChargeLogTable(ManageCompanyRequest $request)
	{
		return Datatables::of($this->repository->getChargeLogTable($request['company_id']))
		->editColumn('company_id',function($charge)
		{
			return $charge->company->name;
		})
		->editColumn('user_id',function($charge)
		{
			return $charge->user->first_name ." ". $charge->user->last_name;
		})
		->addColumn('actions', function ($transfer) {
				return $transfer->action_buttons;
		})
		->escapeColumns(['title'])
		->make(true);
	}
	
}
