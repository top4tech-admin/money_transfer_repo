<?php

namespace App\Http\Controllers\Backend\Companys;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Companys\CreateCompanyRequest;
use App\Http\Requests\Backend\Companys\DeleteCompanyRequest;
use App\Http\Requests\Backend\Companys\EditCompanyRequest;
use App\Http\Requests\Backend\Companys\ManageCompanyRequest;
use App\Http\Requests\Backend\Companys\StoreCompanyRequest;
use App\Http\Requests\Backend\Companys\UpdateCompanyRequest;
use App\Http\Responses\Backend\Company\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Company;
use App\Models\Charge;
use App\Models\MyBox;
use App\Repositories\Backend\CompanysRepository;
use Illuminate\Support\Facades\View;

class CompanysController extends Controller
{
    /**
     * @var \App\Repositories\Backend\CompanysRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\CompanysRepository $repository
     */
    public function __construct(CompanysRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['companys','charges']);
    }

    /**
     * @param \App\Http\Requests\Backend\Companys\ManageCompanyRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageCompanyRequest $request)
    {
        return new ViewResponse('backend.companys.index');
    }

    /**
     * @param \App\Http\Requests\Backend\Companys\CreateCompanyRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreateCompanyRequest $request)
    {
        return new ViewResponse('backend.companys.create');
    }

    /**
     * @param \App\Http\Requests\Backend\Companys\StoreCompanyRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreCompanyRequest $request)
    {
        $this->repository->create($request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.companys.index'), ['flash_success' => 'تم انشاء company بنجاح']);
    }

    /**
     * @param \App\Models\Company $company
     * @param \App\Http\Requests\Backend\Companys\EditCompanyRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(Company $company, EditCompanyRequest $request)
    {
        return new EditResponse($company);
    }

    /**
     * @param \App\Models\Company $company
     * @param \App\Http\Requests\Backend\Companys\UpdateCompanyRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Company $company, UpdateCompanyRequest $request)
    {
        $this->repository->update($company, $request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.companys.index'), ['flash_success' => 'تم تحديث company بنجاح']);
    }

    /**
     * @param \App\Models\Company $company
     * @param \App\Http\Requests\Backend\Companys\DeleteCompanyRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Company $company, DeleteCompanyRequest $request)
    {
        $this->repository->delete($company);

        return new RedirectResponse(route('admin.companys.index'), ['flash_success' => 'تم حذف company بنجاح']);
    }
	
	/**
     * @param \App\Http\Requests\Backend\Auth\User\ManageCompanyRequest $request
     * @param \App\Models\Company $company
     *
     * @return mixed
     */
    public function show(ManageCompanyRequest $request, Company $company)
    {
        return view('backend.companys.show')
            ->withCompany($company);
    }
	
	public function chargeBalance(ManageCompanyRequest $request, Company $company)
    {
        return view('backend.companys.charge-balance')
            ->withCompany($company);
    }
	
	public function charge(ManageCompanyRequest $request, Company $company)
    {
        $this->repository->chargeBalance($company, $request->except(['_token', '_method']));
		
		return new RedirectResponse(route('admin.companys.charge.show',$company), ['flash_success' => 'تم الشحن بنجاح']);
    }
	
	public function chargeBox(ManageCompanyRequest $request, MyBox $mybox)
    {
        $this->repository->chargeBoxBalance($mybox, $request->except(['_token', '_method']));
		
		return new RedirectResponse(route('admin.mybox.charge.show'), ['flash_success' => 'تم الشحن بنجاح']);
    }
	
	public function chargeLogView(ManageCompanyRequest $request, $company_id)
    {
        return view('backend.companys.charges-log')
            ->with(['company_id' => $company_id]);
    }
	
    public function updateCharge(Charge $charge, UpdateCompanyRequest $request)
    {
        $this->repository->updateCompanyCharge($charge, $request->except(['_token', '_method']));

		return redirect()->back()->with(['flash_success' => 'تم تحديث  قيمة الشحن بنجاح']);
    }
	
	public function destroyCharge(Charge $charge, DeleteCompanyRequest $request)
    {
        $this->repository->deleteCompanyCharge($charge);
		
		return redirect()->back()->with(['flash_success' => 'تم حذف قيمة الشحن بنجاح']);
    }
	
}

