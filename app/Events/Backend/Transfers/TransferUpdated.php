<?php

namespace App\Events\Backend\Transfers;

use Illuminate\Queue\SerializesModels;

/**
 * Class TransferUpdated.
 */
class TransferUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $transfer;

    /**
     * @param $transfer
     */
    public function __construct($transfer)
    {
        $this->transfer = $transfer;
    }
}
