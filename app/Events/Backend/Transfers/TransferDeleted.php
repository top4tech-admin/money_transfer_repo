<?php

namespace App\Events\Backend\Transfers;

use Illuminate\Queue\SerializesModels;

/**
 * Class TransfersDeleted.
 */
class TransferDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $transfer;

    /**
     * @param $transfer
     */
    public function __construct($transfer)
    {
        $this->transfer = $transfer;
    }
}
