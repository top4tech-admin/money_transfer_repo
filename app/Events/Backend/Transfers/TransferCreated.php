<?php

namespace App\Events\Backend\Transfers;

use Illuminate\Queue\SerializesModels;

/**
 * Class TransferCreated.
 */
class TransferCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $transfer;

    /**
     * @param $transfer
     */
    public function __construct($transfer)
    {
        $this->transfer = $transfer;
    }
}
