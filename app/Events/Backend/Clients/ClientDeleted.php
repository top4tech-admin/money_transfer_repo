<?php

namespace App\Events\Backend\Clients;

use Illuminate\Queue\SerializesModels;

/**
 * Class ClientsDeleted.
 */
class ClientDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $client;

    /**
     * @param $client
     */
    public function __construct($client)
    {
        $this->client = $client;
    }
}
