<?php

namespace App\Models;

use App\Models\Traits\ModelAttributes;
use App\Models\Traits\Attributes\ChargeAttributes;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Relationships\ChargeRelationships;

class Charge extends BaseModel
{
    use  ModelAttributes, ChargeRelationships, ChargeAttributes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
	protected $table = 'charge';
	
    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
		'user_id',
		'amount',
    ];
	
}
    