<?php

namespace App\Models;

use App\Models\Traits\Attributes\TransferAttributes;
use App\Models\Traits\ModelAttributes;
use App\Models\Traits\Relationships\TransferRelationships;

class MyBox extends BaseModel
{
    use  ModelAttributes, TransferRelationships, TransferAttributes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
	protected $table = 'mybox';
	
    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
		'logo',
		'all_asset',
		'all_required', // مطاليب غير مسلمة
		'my_asset'
    ];
}
    