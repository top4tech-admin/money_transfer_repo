<?php

namespace App\Models\Traits\Relationships;

use App\Models\Auth\User;
use App\Models\Charge;

trait CompanyRelationships
{
	public function charges(){
		return $this->hasMany(Charge::class);
	}
}
    