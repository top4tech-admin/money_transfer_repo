<?php

namespace App\Models\Traits\Relationships;

use App\Models\Client;
use App\Models\Company;
use App\Models\Auth\User;

trait TransferRelationships
{
	/**
     * Page belongs to relationship with user.
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
	
	/**
     * Page belongs to relationship with user.
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
	
	//-----------------------------
	public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
	
	public function deliver()
    {
        return $this->belongsTo(User::class, 'delivered_by');
    }
	//-----------------------------
}
    