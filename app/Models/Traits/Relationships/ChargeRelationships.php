<?php

namespace App\Models\Traits\Relationships;

use App\Models\Auth\User;
use App\Models\Company;

trait ChargeRelationships
{
	public function company(){
		return $this->belongsTo(Company::class, 'company_id');
	}
	
	public function user(){
		return $this->belongsTo(User::class, 'user_id');
	}
}
    