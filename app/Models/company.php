<?php

namespace App\Models;

use App\Models\Traits\Attributes\CompanyAttributes;
use App\Models\Traits\ModelAttributes;
use App\Models\Traits\Relationships\CompanyRelationships;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends BaseModel
{
    use  ModelAttributes, CompanyRelationships, CompanyAttributes, SoftDeletes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
		'logo',
		'asset', //الرصيد
		'required'// مطاليب غير مسلمة
    ];
}
    