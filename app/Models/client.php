<?php

namespace App\Models;

use App\Models\Traits\Attributes\ClientAttributes;
use App\Models\Traits\ModelAttributes;
use App\Models\Traits\Relationships\ClientRelationships;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends BaseModel
{
    use  ModelAttributes, ClientRelationships, ClientAttributes, softDeletes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
		'identification',
		'finger_print',
		'mother_name',
		'birthday',
		'phone'
    ];
}
    