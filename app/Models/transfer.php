<?php

namespace App\Models;

use App\Models\Traits\Attributes\TransferAttributes;
use App\Models\Traits\ModelAttributes;
use App\Models\Traits\Relationships\TransferRelationships;

class Transfer extends BaseModel
{
    use  ModelAttributes, TransferRelationships, TransferAttributes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
		'client_id',
		'amount',
		'note',
		'created_by',
		'delivered_by',
		'status',
		'delivered_at'
    ];
}
    