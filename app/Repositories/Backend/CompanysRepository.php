<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Companys\CompanyCreated;
use App\Events\Backend\Companys\CompanyDeleted;
use App\Events\Backend\Companys\CompanyUpdated;
use App\Exceptions\GeneralException;
use App\Models\Company;
use App\Models\MyBox;
use App\Models\Charge;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class CompanysRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Company::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'created_at',
        'updated_at',
        'name',
		'logo',
		'asset',
		'required'
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
				'id',
                'name',
				'logo',
				'asset',
				'required',
				'created_at'
            ])
			->orderBy('created_at', 'desc');
    }

	public function getChargeLogTable($company_id)
    {
		$charge = new Charge();
		return $charge->query()
			->where('company_id',$company_id)
            ->select([
				'id',
                'company_id',
				'user_id',
				'amount',
				'created_at'
            ])
			->orderBy('created_at', 'desc');
	}
	
    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
		
        if ($company = Company::create($input)) {
            event(new CompanyCreated($company));

			$my_box = MyBox::first();
			$all_asset = $my_box->all_asset +  $input['asset'];
			
			if(!$my_box->update(['id' => $my_box->id, 'all_asset' => $all_asset ]))
			{
				throw new GeneralException('مشكلة في تحديث قيمة المطاليب الخاصة بالصندوق العام');
			}
			
				return $company->fresh();
			}

        throw new GeneralException('حدث مشكلة اثناء  انشاء الشركة, الرجاء حاول مجددا');
    }

    /**
     * Update Company.
     *
     * @param \App\Models\Company $company
     * @param array $input
     */
    public function update(Company $company, array $input)
    {

        if ($company->update($input)) {
            event(new CompanyUpdated($company));

            return $company;
        }

        throw new GeneralException(
            'حدث مشكلة اثناء  تحديث company'
        );
    }

    /**
     * @param \App\Models\Company $company
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Company $company)
    {
        if ($company->delete()) {
            event(new CompanyDeleted($company));

            return true;
        }

        throw new GeneralException('حدث مشكلة اثناء حذف company');
    }
	
	public function chargeBalance(Company $company, array $input)
    {
		$input['asset'] = $company->asset + $input['value'];
		$my_box = MyBox::first();
		
		$input['all_asset'] = $my_box->all_asset + $input['value'];
		$my_box->update($input);
		
		if (!Charge::create(
				['company_id'=>$company->id,
				'user_id'=>auth()->user()->id,
				'amount'=>$input['value']])) {
					
			throw new GeneralException(
				'حدثت مشكلة تسجيل الرصيد '
			);
			
        }
		
        if ($company->update($input)) {

            return $company;
        }

        throw new GeneralException(
            'حدثت مشكلة اثناء شحن الرصيد'
        );
    }
	
	public function chargeBoxBalance(MyBox $my_box, array $input)
    {
		$input['all_asset'] = $my_box->all_asset + $input['value'];
		$input['my_asset'] = $my_box->my_asset + $input['value'];
		
		if (!Charge::create(
				['user_id'=>auth()->user()->id,
				'amount'=>$input['value']])) {
					
			throw new GeneralException(
				'حدثت مشكلة تسجيل الرصيد '
			);
			
        }
		
        if ($my_box->update($input)) {

            return $my_box;
        }

        throw new GeneralException(
            'حدثت مشكلة اثناء شحن الرصيد'
        );
    }
	
	public function updateCompanyCharge(Charge $charge, array $input)
	{
		// عند تحديث الشحن ستم حذف القيمة القديمة من رصيد الشركة و الصندوق و ثم اضافة القيمة الجديدة اليهم
		$my_box = MyBox::first();
		
		$input['all_asset'] = $my_box->all_asset - $charge->amount + $input['amount'];
		$my_box->update($input);
		
		$company = Company::find($charge->company_id);
		$new_val = $company->asset - $charge->amount + $input['amount'];
		
        if ($company->update(["asset" => $new_val])) {
			$charge->update(["amount" => $input['amount']]);
            return $charge;
        }

        throw new GeneralException(
            'حدثت مشكلة اثناء تحديث شحن الرصيد'
        );
    }
	
	public function deleteCompanyCharge(Charge $charge)
	{
		// عند حذف قيمة الشحن سيتم انقاص هذه القيمة من الصندوق و من حساب الششركة
		$my_box = MyBox::first();
		
		$input['all_asset'] = $my_box->all_asset - $charge->amount;
		$my_box->update($input);
		
		$company = Company::find($charge->company_id);
		$new_val = $company->asset - $charge->amount;
		
        if ($company->update(["company" => $company->id ,"asset" => $new_val])) {
			$charge->delete();
			return true;
        }

        throw new GeneralException('حدث مشكلة اثناء حذف قيمة الشحن');
    }
	
}
   