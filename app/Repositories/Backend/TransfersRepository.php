<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Transfers\TransferCreated;
use App\Events\Backend\Transfers\TransferDeleted;
use App\Events\Backend\Transfers\TransferUpdated;
use App\Exceptions\GeneralException;
use App\Models\Transfer;
use App\Models\Company;
use App\Models\MyBox;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

class TransfersRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Transfer::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'created_at',
        'updated_at',
        'company_id',
		'client_id',
		'amount',
		'status'
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

	public function retriveTransfersList(array $options = [], int $id)
	{
		$perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
			->where('status','F') // 'f' means transfer is not recived by client
			->where('client_id',$id) 
            ->with([
                'client',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
	}
	
    /**
     * @return mixed
     */
    public function getForDataTable($date = null, $companies = null)
    {
		// يعيد الحوالات المسلمة و غير المسلمة
		if($date == null && empty($companies)){
        return $this->query()
			->leftjoin('clients','clients.id','=','transfers.client_id')
            ->select([
				'clients.id as client_id',
				'transfers.id',
                'transfers.company_id',
				'transfers.amount',
				'transfers.status',
				'clients.name as client_name',
				'clients.mother_name as client_mother',
				'clients.phone as client_phone',
				'clients.birthday as client_birthday',
				'transfers.created_at',
				'transfers.delivered_at'
            ])
			->orderBy('created_at', 'desc');
			}else if($date != null && empty($companies)){
				// يعيد الحوالات التي تم انشاءهااو تم تسليمها بتاريخ محدد
			return $this->query()
			->whereDate('transfers.created_at','=',$date )
			->orWhereDate('transfers.delivered_at','=',$date )
			->leftjoin('clients','clients.id','=','transfers.client_id')
            ->select([
				'clients.id as client_id',
				'transfers.id',
                'transfers.company_id',
				'transfers.amount',
				'transfers.status',
				'clients.name as client_name',
				'clients.mother_name as client_mother',
				'clients.phone as client_phone',
				'clients.birthday as client_birthday',
				'transfers.created_at',
				'transfers.delivered_at'
            ])
			->orderBy('created_at', 'desc');
			} else if($date == null && !empty($companies)){
				return $this->query()
				->whereIn('transfers.company_id',$companies)
				->leftjoin('clients','clients.id','=','transfers.client_id')
				->select([
					'clients.id as client_id',
					'transfers.id',
					'transfers.company_id',
					'transfers.amount',
					'transfers.status',
					'clients.name as client_name',
					'clients.mother_name as client_mother',
					'clients.phone as client_phone',
					'clients.birthday as client_birthday',
					'transfers.created_at',
					'transfers.delivered_at'
				])
				->orderBy('created_at', 'desc');
			}else{
				return $this->query()
				->whereDate('transfers.created_at','=',$date )
				->whereIn('transfers.company_id',$companies)
				->orWhereDate('transfers.delivered_at','=',$date )
				->leftjoin('clients','clients.id','=','transfers.client_id')
				->whereIn('transfers.company_id',$companies)
				->select([
					'clients.id as client_id',
					'transfers.id',
					'transfers.company_id',
					'transfers.amount',
					'transfers.status',
					'clients.name as client_name',
					'clients.mother_name as client_mother',
					'clients.phone as client_phone',
					'clients.birthday as client_birthday',
					'transfers.created_at',
					'transfers.delivered_at'
				])
				->orderBy('created_at', 'desc');
			}
    }

	// Get Unrecived Transfers for table
	public function getTransferStatusF($date = null)
		{
			// يعيد الحوالات غير المسلمة
			if($date == null){
			return $this->query()
				->where('status','F')
				->leftjoin('clients','clients.id','=','transfers.client_id')
				->select([
					'clients.id as client_id',
					'transfers.id',
					'transfers.company_id',
					'transfers.amount',
					'transfers.status',
					'clients.name as client_name',
					'clients.mother_name as client_mother',
					'clients.phone as client_phone',
					'clients.birthday as client_birthday',
					'transfers.created_at'
				])
				->orderBy('created_at', 'desc');
				}else{
				//يعيد الحوالات غير المسلمة التي تم انشاؤها بتاريخ محدد
				return $this->query()
				->where('status','F')
				->whereDate('transfers.created_at','=',$date )
				->leftjoin('clients','clients.id','=','transfers.client_id')
				->select([
					'clients.id as client_id',
					'transfers.id',
					'transfers.company_id',
					'transfers.amount',
					'transfers.status',
					'clients.name as client_name',
					'clients.mother_name as client_mother',
					'clients.phone as client_phone',
					'clients.birthday as client_birthday',
					'transfers.created_at'
				])
				->orderBy('created_at', 'desc');
			}
		}
	
	// Get Recived Transfers for table
	public function getTransferStatusT($date = null, $companies = null)
		{
			//يعيد جميع الحوالات المسلمة 
			if($date == null && empty($companies)){
			return $this->query()
				->where('status','T')
				->leftjoin('clients','clients.id','=','transfers.client_id')
				->select([
					'clients.id as client_id',
					'transfers.id',
					'transfers.company_id',
					'transfers.amount',
					'transfers.status',
					'clients.name as client_name',
					'clients.mother_name as client_mother',
					'clients.phone as client_phone',
					'clients.birthday as client_birthday',
					'transfers.created_at',
					'transfers.delivered_at'
				])
			->orderBy('delivered_at', 'desc');
			}else if($date != null && empty($companies))
			{
				//يعيد جميع الحوالات المسلمة بتاريخ محدد
				return $this->query()
				->where('status','T')
				//->where('delivered_at',$date)Carbon::today()
				->whereDate('delivered_at','=',$date )
				->leftjoin('clients','clients.id','=','transfers.client_id')
				->select([
					'clients.id as client_id',
					'transfers.id',
					'transfers.company_id',
					'transfers.amount',
					'transfers.status',
					'clients.name as client_name',
					'clients.mother_name as client_mother',
					'clients.phone as client_phone',
					'clients.birthday as client_birthday',
					'transfers.created_at',
					'transfers.delivered_at'
				])
			->orderBy('delivered_at', 'desc');
			}else if($date == null && !empty($companies))
			{
				//يعيد جميع الحوالات المسلمة بتاريخ محدد
				return $this->query()
				->where('status','T')
				//->where('delivered_at',$date)Carbon::today()
				->whereIn('company_id',$companies)
				->leftjoin('clients','clients.id','=','transfers.client_id')
				->select([
					'clients.id as client_id',
					'transfers.id',
					'transfers.company_id',
					'transfers.amount',
					'transfers.status',
					'clients.name as client_name',
					'clients.mother_name as client_mother',
					'clients.phone as client_phone',
					'clients.birthday as client_birthday',
					'transfers.created_at',
					'transfers.delivered_at'
				])
			->orderBy('delivered_at', 'desc');
			}else
			{
				//يعيد جميع الحوالات المسلمة بتاريخ محدد
				return $this->query()
				->where('status','T')
				//->where('delivered_at',$date)Carbon::today()
				->whereDate('delivered_at','=',$date )
				->whereIn('company_id',$companies)
				->leftjoin('clients','clients.id','=','transfers.client_id')
				->select([
					'clients.id as client_id',
					'transfers.id',
					'transfers.company_id',
					'transfers.amount',
					'transfers.status',
					'clients.name as client_name',
					'clients.mother_name as client_mother',
					'clients.phone as client_phone',
					'clients.birthday as client_birthday',
					'transfers.created_at',
					'transfers.delivered_at'
				])
			->orderBy('delivered_at', 'desc');
			}
		}
		
	// Get Recived Transfers for table
	public function getTransferforCompany($com_id)
		{
			// يعيد كل الحوالات الخاصة بشركة محددة
			return $this->query()
				->where('company_id', $com_id)
				->leftjoin('clients','clients.id','=','transfers.client_id')
				->select([
					'clients.id as client_id',
					'transfers.id',
					'transfers.company_id',
					'transfers.amount',
					'transfers.status',
					'clients.name as client_name',
					'clients.mother_name as client_mother',
					'clients.phone as client_phone',
					'clients.birthday as client_birthday',
					'transfers.created_at'
				])
				->orderBy('created_at', 'desc');
		}
		
    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
		// عند ادخال اي حوالة جديدة سيتم اضافة قيمة الحوالة الى المطاليب في الصندوق العام و كذلك الى المطاليب في حساب الشركة
		$com = Company::find($input['company_id']);
		$my_box = MyBox::first();
		
		$com_required = $com->required + $input['amount'];
		if(!$com->update(['id' => $input['company_id'],'required' => $com_required ]))
		{
			throw new GeneralException('مشكلة في تحديث قيمة المطاليب الخاصة بالشركة');
		}
		
		$box_required = $my_box->all_required + $input['amount'];
		if(!$my_box->update(['id' => $my_box->id, 'all_required' => $box_required ]))
		{
			throw new GeneralException('مشكلة في تحديث قيمة المطاليب الخاصة بالصندوق العام');
		}

        if ($transfer = Transfer::create($input))
		{
            event(new TransferCreated($transfer));

            return $transfer->fresh();
        }

        throw new GeneralException('حدث مشكلة اثناء  انشاء transfer');
    }

    /**
     * Update Transfer.
     *
     * @param \App\Models\Transfer $transfer
     * @param array $input
     */
    public function update(Transfer $transfer, array $input)
    {
		// عند تعديل قيمة الحوالة نقوم بحذف القيمة القديمة و اضافة القيمة الجديدة للحوالة في المطاليب عند الشركة و كذلك في الصندوق
		$com = Company::find($input['company_id']);
		$my_box = MyBox::first();
		
		$com_required = $com->required - $input['old_amount'] + $input['amount'];
		if(!$com->update(['id' => $input['company_id'],'required' => $com_required ]))
		{
			throw new GeneralException('مشكلة في تحديث قيمة المطاليب الخاصة بالشركة');
		}
		
		$box_required = $my_box->all_required - $input['old_amount'] + $input['amount'];
		if(!$my_box->update(['id' => $my_box->id, 'all_required' => $box_required ]))
		{
			throw new GeneralException('مشكلة في تحديث قيمة المطاليب الخاصة بالصندوق العام');
		}
		
        if ($transfer->update($input)) {
            event(new TransferUpdated($transfer));

            return $transfer;
        }

        throw new GeneralException(
            'حدث مشكلة اثناء  تحديث transfer'
        );
    }

	public function reciveTransfer(Transfer $transfer, array $input)
    {
		// عند تسليم حوالة تنقص المطاليب و كذلك ينقص رصيد الشركة و الرصيد من الصندوق
		$com = Company::find($transfer->company_id);
		$com_required = $com->required -  $transfer->amount;
		$asset = $com->asset -  $transfer->amount;
		if(!$com->update(['id' => $transfer->company_id,'required' => $com_required,'asset' => $asset ]))
		{
			throw new GeneralException('مشكلة في تحديث قيمة المطاليب الخاصة بالشركة');
		}
		
		$my_box = MyBox::first();
		$box_required = $my_box->all_required -  $transfer->amount;
		$all_asset = $my_box->all_asset -  $transfer->amount;
		if(!$my_box->update(['id' => $my_box->id, 'all_required' => $box_required,'all_asset' => $all_asset ]))
		{
			throw new GeneralException('مشكلة في تحديث قيمة المطاليب الخاصة بالصندوق العام');
		}
			
		if ($transfer->update($input))
		{
			return $transfer;
        }
		
		throw new GeneralException(
            'حدثت مشكلة اثناء تسليم الحوالة'
        );
    }
	
	//  تراجع عن تسليم حوالة (متاح للادمن فقط)
	public function rollbackTransfer(Transfer $transfer, array $input)
    {
		// عند التراجع عن تسليم حوالة سيتم اضافة قيمتها الى الصندوق و المطاليب  العامة و كذلك الى حساب الشركة و مطاليبها
		$com = Company::find($transfer->company_id);
		$com_required = $com->required +  $transfer->amount;
		$asset = $com->asset +  $transfer->amount;
		if(!$com->update(['id' => $transfer->company_id,'required' => $com_required,'asset' => $asset ]))
		{
			throw new GeneralException('مشكلة في تحديث قيمة المطاليب و راس مال الخاص بالشركة');
		}
		
		$my_box = MyBox::first();
		$box_required = $my_box->all_required +  $transfer->amount;
		$all_asset = $my_box->all_asset +  $transfer->amount;
		if(!$my_box->update(['id' => $my_box->id, 'all_required' => $box_required,'all_asset' => $all_asset ]))
		{
			throw new GeneralException('مشكلة في تحديث قيمة المطاليب وراس المال الخاص بالصندوق العام');
		}
		$input['status'] = 'F';
		$input['delivered_at'] = NULL;
		$input['delivered_by'] = NULL;
		if ($transfer->update($input))
		{
			return $transfer;
        }
		
		throw new GeneralException(
            'حدثت مشكلة اثناء التراجع عن تسليم الحوالة'
        );
    }
    /**
     * @param \App\Models\Transfer $transfer
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Transfer $transfer)
    {
		// عند حذف حوالة يتم انقاص المطاليب بمقدار قيمة الحوالة
		$com = Company::find($transfer['company_id']);
		$my_box = MyBox::first();
		
		$com_required = $com->required - $transfer->amount;
		$box_required = $my_box->all_required - $transfer->amount;
		
		if(!$com->update(['id' => $transfer->company_id,'required' => $com_required]))
		{
			throw new GeneralException('مشكلة في تحديث قيمة المطاليب الخاصة بالشركة');
		}
		
		if(!$my_box->update(['id' => $my_box->id, 'all_required' => $box_required]))
		{
			throw new GeneralException('مشكلة في تحديث قيمة المطاليب الخاصة بالصندوق العام');
		}
		
        if ($transfer->delete()) {
            event(new TransferDeleted($transfer));

            return true;
        }

        throw new GeneralException('حدث مشكلة اثناء حذف transfer');
    }
}
   