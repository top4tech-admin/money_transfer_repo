<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Clients\ClientCreated;
use App\Events\Backend\Clients\ClientDeleted;
use App\Events\Backend\Clients\ClientUpdated;
use App\Exceptions\GeneralException;
use App\Models\Client;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class ClientsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Client::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'created_at',
        'updated_at',
        'name',
		'mother_name',
		'birthday',
		'phone'
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

	/**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function RetrieveClient(array $options = [], int $id)
    {
        $perProduct = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            //->where('status',0) // avilable products => 0 , disabled products => 1
            ->where('id',$id) 
           // ->orderBy('sort')
            ->orderBy('updated_at', $order)
            ->orderBy($orderBy, $order);

        if ($perProduct == -1) {
            return $query->get();
        }

        return $query->paginate($perProduct);
    }

    /**
     * Retrieve List when search for product by its name.
     *
     * @var array
     * @return Collection
     */
    public function RetrieveSearchList(array $options = [], String $client_name)
    {
        $perProduct = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
          //  ->where('status',0) // avilable products => 0 , disabled products => 1
            ->where('name','like',"%{$client_name}%") 
           // ->orderBy('sort')
            ->orderBy('updated_at', $order)
            ->orderBy($orderBy, $order);

        if ($perProduct == -1) {
            return $query->get();
        }

        return $query->paginate($perProduct);
    }
	
    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
				'id',
                'name',
				'mother_name',
				'birthday',
				'phone',
				'created_at'
            ])
			->orderBy('created_at', 'desc');
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
		if ( empty($input['identification']) ) { // will check for empty string 
			$input['identification'] = " ";
		}
		
		if ( empty($input['finger_print']) ) { // will check for empty string 
			$input['finger_print'] = " ";
		}
		
        if ($client = Client::create($input)) {
            event(new ClientCreated($client));

            return $client->fresh();
        }

        throw new GeneralException('حدث مشكلة اثناء  انشاء client');
    }

    /**
     * Update Client.
     *
     * @param \App\Models\Client $client
     * @param array $input
     */
    public function update(Client $client, array $input)
    {

        if ($client->update($input)) {
            event(new ClientUpdated($client));

            return $client;
        }

        throw new GeneralException(
            'حدث مشكلة اثناء  تحديث client'
        );
    }

    /**
     * @param \App\Models\Client $client
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Client $client)
    {
        if ($client->delete()) {
            event(new ClientDeleted($client));

            return true;
        }

        throw new GeneralException('حدث مشكلة اثناء حذف client');
    }
}
   