<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Costs\CostCreated;
use App\Events\Backend\Costs\CostDeleted;
use App\Events\Backend\Costs\CostUpdated;
use App\Exceptions\GeneralException;
use App\Models\Cost;
use App\Models\MyBox;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class CostsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Cost::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'created_at',
        'updated_at',
        'note',
		'type',
		'amount'
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
				'id',
                'note',
				'type',
				'amount',
				'created_at'
            ])
			->orderBy('created_at', 'desc');
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
		$my_box = MyBox::first();
		$arr['my_asset'] = $my_box->my_asset - $input['amount'];
		$arr['all_asset'] = $my_box->all_asset - $input['amount'];
		$my_box->update($arr);
		
        if ($cost = Cost::create($input)) {
            event(new CostCreated($cost));

            return $cost->fresh();
        }

        throw new GeneralException('حدث مشكلة اثناء  انشاء cost');
    }

    /**
     * Update Cost.
     *
     * @param \App\Models\Cost $cost
     * @param array $input
     */
    public function update(Cost $cost, array $input)
    {
		// $cost->amount == $input['old_amount']
		
		$my_box = MyBox::first();
		$arr['my_asset'] = $my_box->my_asset + $input['old_amount']- $input['amount'];
		$arr['all_asset'] = $my_box->all_asset + $input['old_amount'] - $input['amount'];
		$my_box->update($arr);
		
        if ($cost->update($input)) {
            event(new CostUpdated($cost));

            return $cost;
        }

        throw new GeneralException(
            'حدث مشكلة اثناء  تحديث cost'
        );
    }

    /**
     * @param \App\Models\Cost $cost
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Cost $cost)
    {
        if ($cost->delete()) {
            event(new CostDeleted($cost));

            return true;
        }

        throw new GeneralException('حدث مشكلة اثناء حذف cost');
    }
}
   