<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Mybox extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('mybox', function (Blueprint $table) {
		$table->bigIncrements('id');
		$table->string('name')->default('شركتي');
		$table->string('logo')->nullable();
		$table->double('all_required')->default(0); //  المطاليب من كل الشركات
		$table->double('all_asset')->default(0); // الاموال التابعة للشركات في الصندوق
		$table->double('my_asset')->default(0); // الاموال الخاصة بي الموجودة في الصندوق
		$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('mybox');
    }
}
