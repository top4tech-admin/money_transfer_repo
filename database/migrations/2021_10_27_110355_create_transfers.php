<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransfers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('company_id')->unsigned();
			$table->bigInteger('client_id')->unsigned();
			$table->double('amount');
			$table->text('note')->nullable();
			$table->char('status');
			$table->bigInteger('created_by')->unsigned();
			$table->bigInteger('delivered_by')->nullable();
			$table->timestamp('delivered_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
