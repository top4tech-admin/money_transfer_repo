<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
			$table->string('mother_name')->nullable();
			$table->date('birthday')->nullable();
			$table->string('phone')->uniqe()->nullable()->index();;
			$table->binary('finger_print')->uniqe()->nullable(); // binary --> blob
			$table->string('identification')->uniqe()->nullable();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
