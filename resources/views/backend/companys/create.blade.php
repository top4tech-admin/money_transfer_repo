@extends('backend.layouts.app')

@section('title', 'ادارة company | انشاء company')

@section('breadcrumb-links')
    @include('backend.companys.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.companys.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission', 'files' => true]) }}

    <div class="card">
        @include('backend.companys.form')
        @include('backend.components.footer-buttons', ['cancelRoute' => 'admin.companys.index'])
    </div><!--card-->
    {{ Form::close() }}
@endsection
