@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.view'))

@section('breadcrumb-links')
	@include('backend.companys.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                     شحن رصيد لشركة {{$company->name}}
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
		
            <div class="col-6">

				<form action="{{route('admin.companys.charge',['company' => $company])}}" method="POST" class="form-group row">
				@csrf
				{{ Form::label('asset', 'القيمة', ['class' => 'col-md-2 from-control-label required']) }}
				<div class="col-md-5">
					<input min="0" type="number" name="value" class="form-control" placeholder="0" required />
				</div>
				<!--col-->
				<div class="col-md-1">
				</div>
				<div class="col-md-3">		
					<input type="submit" name="asset" value="شحن الان" />
				</div>
				</form>
				
				
			</div><!--col-->
			
			<div class="col-6">
				<ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true"><i class="fas fa-user"></i> @lang('labels.backend.access.users.tabs.titles.overview')</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">
                        @include('backend.companys.show.tabs.overview')
                    </div><!--tab-->
                </div><!--tab-content-->
			</div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

</div><!--card-->
@endsection
