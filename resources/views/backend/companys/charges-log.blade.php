@extends('backend.layouts.app')

@section('title', app_name() . ' | ادراة الشحن')

@section('breadcrumb-links')
@include('backend.companys.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">

		<div class="row justify-content-between">
            <div class="col-sm-4">
                <h4 class="card-title mb-0">
                    ادراة الشحن  
                </h4>
            </div>
			<div class="col-sm-4">
                <h6 class="card-title mb-0">
                    العدد المحدد:   <span id="span-trans-number"></span>   
                </h6>
            </div>
			<div class="col-sm-4">
                <h6 class="card-title mb-0">
                    المجموع: <span id="span-trans-amount"></span> 
                </h6>
            </div>
            <!--col-->
        </div>
        <!--row-->
		
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="charges-table" class="table" data-ajax_url="{{ route('admin.companys.charge.log.get', ['company_id' =>$company_id]) }}">
                        <thead>
                            <tr>
							<th>اسم الشركة</th> 
							<th>قام بالشحن</th> 
							<th>القيمة</th> 
							<th>التاريخ</th>
							<th>الاجراءات</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Charges.list.init();
    });
</script>
@endsection
