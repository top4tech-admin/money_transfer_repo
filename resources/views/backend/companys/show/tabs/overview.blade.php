<div class="col">
<div class="table-responsive">
    <table class="table table-hover">
        <tr>
            <th>Logo</th>
            <td><img src="{{ $company->logo }}" class="user-profile-image" /></td>
        </tr>

        <tr>
            <th>اسم الشركة</th>
            <td>{{ $company->name }}</td>
        </tr>

        <tr>
            <th>الموجودات</th>
            <td>{{ $company->asset }}</td>
        </tr>

        <tr>
            <th>المطاليب</th>
            <td>{{$company->required}}</td>
        </tr>
		
		<tr>
            <th>الميزان</th>
            <td>{{$company->asset - $company->required}}</td>
        </tr>

		<tr>
            <th>تاريخ انشاء الشركة</th>
            <td>{{$company->created_at}}</td>
        </tr>

    </table>
</div>
</div><!--table-responsive-->
