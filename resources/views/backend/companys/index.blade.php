@extends('backend.layouts.app')

@section('title', app_name() . ' | ادراة companys')

@section('breadcrumb-links')
@include('backend.companys.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">

		<div class="row justify-content-between">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    ادراة الشركات  
                </h4>
            </div>
			<div class="col-sm-2">
                <h4 class="card-title mb-0">
                    <a href="{{ route('admin.companys.create') }}" class="btn btn-success">انشاء شركة جديدة</a> 
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->
		
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="companys-table" class="table" data-ajax_url="{{ route("admin.companys.get") }}">
                        <thead>
                            <tr>
                                
							<th>الاسم</th> 
							<th>اللوغو</th> 
							<th>الرصيد</th> 
							<th>مطاليب</th> 
                                <th>{{ trans('labels.backend.access.pages.table.createdat') }}</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Companys.list.init();
    });
</script>
@endsection
