@extends('backend.layouts.app')

@section('title', 'companys management | تعديل company'))

@section('breadcrumb-links')
    @include('backend.companys.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::model($company, ['route' => ['admin.companys.update', $company], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role', 'files' => true]) }}

    <div class="card">
        @include('backend.companys.form')
        @include('backend.components.footer-buttons', [ 'cancelRoute' => 'admin.companys.index', 'id' => $company->id ])
    </div><!--card-->
    {{ Form::close() }}
@endsection
