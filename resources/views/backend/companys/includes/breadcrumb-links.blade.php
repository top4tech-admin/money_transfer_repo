<li class="breadcrumb-menu">
<div class="btn-group" role="group" aria-label="Button group">
    <div class="dropdown">
        <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">الشركات</a>

        <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
            <a class="dropdown-item" href="{{ route('admin.companys.index') }}">جميع الشركات</a>
            <a class="dropdown-item" href="{{ route('admin.companys.create') }}">'انشاء شركة جديدة</a>
        </div>
    </div><!--dropdown-->

    <!--<a class="btn" href="#">Static Link</a>-->
</div><!--btn-group-->
</li>