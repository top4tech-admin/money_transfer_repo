<div class="card-body">
<div class="row">
    <div class="col-sm-5">
        <h4 class="card-title mb-0">
             ادراة الشركات 
            <small class="text-muted">{{ (isset($company)) ? 'تعديل بيانات الشركة' : 'انشاء شركة جديدة' }}</small>
        </h4>
    </div>
    <!--col-->
</div>
<!--row-->

<hr>

<div class="row mt-4 mb-4">
    <div class="col-6">
        
		<div class="form-group row">
        {{ Form::label('name', 'الاسم', ['class' => 'col-md-2 from-control-label required']) }}

        <div class="col-md-10">
            {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name', 'required' => 'required']) }}
        </div>
        <!--col-->
        </div>
        <!--form-group-->

		<div class="form-group row">
            {{ Form::label('logo', 'اللوغو', ['class' => 'col-md-2 from-control-label required']) }}

            <div class="col-md-10">
                {{ Form::text('logo', null, ['class' => 'form-control', 'placeholder' => 'اللوغو']) }}
            </div>
            <!--col-->
		</div>
		<!--form-group-->
		
		@if(!isset($company))
		<div class="form-group row">
        {{ Form::label('asset', 'الرصيد (السالب للديون)', ['class' => 'col-md-2 from-control-label required']) }}

        <div class="col-md-10">
            {{ Form::number('asset', null, ['class' => 'form-control', 'placeholder' => 'له', 'required' => 'required']) }}
        </div>
        <!--col-->
        </div>
        <!--form-group-->
		
		@endif
		
		@if(isset($company))
			<input type="number" name="required" value="{{$company->required}}" hidden required />
		@else
			<input type="number" name="required" value="0" required hidden />
		@endif

    </div>
    <!--col-->
</div>
<!--row-->
</div>
<!--card-body-->

@section('pagescript')
<script type="text/javascript">
FTX.Utils.documentReady(function() {
    FTX.Pages.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
});
</script>
@stop
