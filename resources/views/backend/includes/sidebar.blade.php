<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
			@if ($logged_in_user->isAdmin())
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/dashboard'))
                }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>
			
			<li class="nav-item nav-dropdown {{
				active_class(Route::is('admin/log-viewer*'), 'open')
			}}">
					<a class="nav-link nav-dropdown-toggle {{
						active_class(Route::is('admin/log-viewer*'))
					}}" href="#">
					<i class="nav-icon fas fa-list"></i> الصندوق
				</a>

				<ul class="nav-dropdown-items">
					<li class="nav-item">
						<a class="nav-link {{
						active_class(Route::is('admin/log-viewer'))
					}}" href="{{ route('admin.mybox.charge.show') }}">
							شحن الصندوق
						</a>
					</li>
				</ul>
			</li>
			
			<li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/company'))
                }}" href="{{ route('admin.companys.index') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    شركات
                </a>
            </li>
			
			<li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/client'))
                }}" href="{{ route('admin.clients.index') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    الزبائن
                </a>
            </li>
			
			<li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/cost'))
                }}" href="{{ route('admin.costs.index') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    مصاريف
                </a>
            </li>
			
			<!-- ToDo -->
			<li class="nav-item hidden">
                <a class="nav-link {{
                    active_class(Route::is('/backup'))
                }}" href="{{ route('admin.backup.db') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    النسخ الاحتياطي
                </a>
            </li>
			
			<li class="nav-title">
                   الحوالات
            </li>
			
			<li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/transfer'))
                }}" href="{{ route('admin.transfers.index') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    جميع الحوالات
                </a>
            </li>
			
			<li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/transfers/unrecived'))
                }}" href="{{ route('admin.transfers.unrecived') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                   حوالات غير مسلمة
                </a>
            </li>
			
			<li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/transfers/recived'))
                }}" href="{{ route('admin.transfers.recived') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    حوالات تم تسليمها
                </a>
            </li>
			@endif
			
			<li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/transfer/create'))
                }}" href="{{ route('admin.transfers.create') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    انشاء حوالة جديدة
                </a>
            </li>
			
			<li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/transfers/recive'))
                }}" href="{{ route('admin.transfers.recive') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    تسليم حوالة
                </a>
            </li>
			
            @if ($logged_in_user->isAdmin())
                <li class="nav-title">
                    @lang('menus.backend.sidebar.system')
                </li>

                <li class="nav-item nav-dropdown {{
                    active_class(Route::is('admin/auth*'), 'open')
                }}">
                    <a class="nav-link nav-dropdown-toggle {{
                        active_class(Route::is('admin/auth*'))
                    }}" href="#">
                        <i class="nav-icon far fa-user"></i>
                        @lang('menus.backend.access.title')

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/user*'))
                            }}" href="{{ route('admin.auth.user.index') }}">
                                @lang('labels.backend.access.users.management')

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/role*'))
                            }}" href="{{ route('admin.auth.role.index') }}">
                                @lang('labels.backend.access.roles.management')
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/permission*'))
                            }}" href="{{ route('admin.auth.permission.index') }}">
                                @lang('labels.backend.access.permissions.management')
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="divider"></li>
				
				<li class="nav-title">
					قسم الصيانة البرمجية
				</li>
				
                <li class="nav-item nav-dropdown {{
                    active_class(Route::is('admin/log-viewer*'), 'open')
                }}">
                        <a class="nav-link nav-dropdown-toggle {{
                            active_class(Route::is('admin/log-viewer*'))
                        }}" href="#">
                        <i class="nav-icon fas fa-list"></i> @lang('menus.backend.log-viewer.main')
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Route::is('admin/log-viewer'))
                        }}" href="{{ route('log-viewer::dashboard') }}">
                                @lang('menus.backend.log-viewer.dashboard')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Route::is('admin/log-viewer/logs*'))
                        }}" href="{{ route('log-viewer::logs.list') }}">
                                @lang('menus.backend.log-viewer.logs')
                            </a>
                        </li>
                    </ul>
                </li>
				<li class="nav-title">
                صلاحيات المستخدم
				</li>
            @endif
			
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
