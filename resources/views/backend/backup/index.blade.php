@extends('backend.layouts.app')

@section('title', app_name() . ' | ادراة companys')

@section('breadcrumb-links')
@include('backend.companys.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">

		<div class="row justify-content-between">
            <div class="col-sm-4">
                <h4 class="card-title mb-0">
                    النسخ الاحتياطي
                </h4>
            </div>
        </div>
        <!--row-->
		
		<div class="col-sm-4">
			<h4 class="card-title mb-0">
				<a href="{{ route('admin.take.db.backup') }}" class="btn btn-success">انشاء نسخة احتياطية عن قاعدة البيانات</a> 
			</h4>
		</div>
		<!--col-->
		
		<div class="col-sm-4">
			<h4 class="card-title mb-0">
				<a href="{{ route('admin.companys.create') }}" class="btn btn-success">استعادة نسخة احتياطية عن قاعدة البيانات</a> 
			</h4>
		</div>
		<!--col-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

