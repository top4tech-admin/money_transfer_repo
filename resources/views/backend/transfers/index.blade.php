@extends('backend.layouts.app')

@section('title', app_name() . ' | ادراة الحوالات')

@section('breadcrumb-links')
@include('backend.transfers.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row justify-content-between">
            <div class="col-sm-2">
                <h4 class="card-title mb-0">
                    جميع الحوالات 
                </h4>
            </div>
			<div class="col-sm-4">
                <h6 class="card-title mb-0">
                    عدد الحوالات المحددة:   <span id="span-trans-number"></span>   
                </h6>
            </div>
			<div class="col-sm-4">
                <h6 class="card-title mb-0">
                    قيمة الحوالات المحددة: <span id="span-trans-amount"></span> 
                </h6>
            </div>
			<div class="col-sm-2">
                <h4 class="card-title mb-0">
                    <a href="{{ route('admin.transfers.create') }}" class="btn btn-success">انشاء حوالة</a> 
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->
		<hr/>
		<div class="row alert alert-danger">
			<div class="col-sm-6">
                <div class="form-check form-check-inline">
					<label class="form-check-label" for="date"> حركة كافة الحوالات بتاريخ </label>
				    <input type="date" id="date" />
				</div>
            </div>

            <div class="col-sm-5">
                <div class="form-check form-check-inline">
					<label class="form-check-label" for="date"> حدد  الشركات </label>
				    <select id="com" multiple>
                        <option disabled selected>جميع الشركات</option>
                        @foreach($companies as $company)
                            <option value="{{$company->id}}">{{$company->name}}</option>
                        @endforeach
                    </select>
                    <button id="selectCompanies" >OK</button>
				</div>
            </div>
        </div>
        <!--row-->
		
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="transfers-table" class="display nowrap" data-ajax_url="{{ route("admin.transfers.get") }}">
                        <thead>
                            <tr>
								<th>الشركة</th> 
								<th>الزبون</th> 
								<th>القيمة</th> 
								<th>الحالة</th> 
								<th>اسم الام</th> 
								<th>موبايل</th> 
								<th>الميلاد</th>
                                <th>{{ trans('labels.backend.access.pages.table.createdat') }}</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->


<!-- Modal -->
<div class="modal fade" id="confirm-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title h1" id="exampleModalLongTitle">تأكيد التسليم</p>
        <button type="button" class="close float-left" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body bac-color-gray">
        <p class="h2"><b>{{$logged_in_user->name}}, هل تريد تسليم الحوالة التالية بالفعل ؟</b></p>
		<hr/>
		<p class="h3"><b>الاسم:</b> <label id="transfr-name"></label></p>
		<p class="h3"><b>القيمة:</b> <label id="transfr-amount"></label></p>
		<p class="h3"><b>الشركة:</b> <label id="transfr-company"></label></p>
		<p class="h4"><b>ملاحظة:</b> <label id="transfr-note"></label></p>

      </div>
      <div class="modal-footer">
		  <div class="float-right alert alert-danger">
			ملاحظة: لا يمكن التراجع عن هذه الخطوة في حال الموافقة
		  </div>
       </div>
	   <div class="modal-footer">
		<form method="POST" accept-charset="UTF-8" class="form-horizontal" role="form" id="form-action" enctype="multipart/form-data">
		@csrf
		<input name="_method" type="hidden" value="PATCH">
		

		<div class="form-group row">
        {{ Form::label('birthday', 'تاريخ الميلاد', ['class' => 'col-12 col-md-3 from-control-label required']) }}

        <div class="col-md-5">
			<input type="date" id="birthday" name="birthday" required />
        </div>
		<div class="col-md-4">
		(سنة | شهر | يوم)
		</div>
        <!--col-->
        </div>
        <!--form-group-->
		
		<div class="form-group row">
        {{ Form::label('mother_name', 'اسم الام', ['class' => 'col-12 col-md-3 from-control-label required']) }}

        <div class="col-12 col-md-9">
            {{ Form::text('mother_name', null, ['class' => 'form-control', 'placeholder' => 'اسم الام', 'required' => 'required']) }}
        </div>
        <!--col-->
        </div>
        <!--form-group-->
		<hr/>
		
		
		<input name='status' type='text' value='T' hidden></input>
		<input id="transfer-id-input" name='id' type='text' value='' hidden></input>
		<button type="button" class="btn btn-secondary size-20" data-dismiss="modal">تراجع</button>
        <button type="submit" id="submitForm" class="btn btn-primary size-20">نعم تأكيد</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!--card-body-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Transfers.list.init();

        $('#date').on('change',function(e){
            let com = $('#com').val();
            console.log(com);
			$("#transfers-table").dataTable().fnDestroy();
			FTX.Transfers.list.init($(this).val(),com );
		});

        $('#selectCompanies').on('click',function(e){
            let com = $('#com').val();
            console.log(com);
            $("#transfers-table").dataTable().fnDestroy();
            FTX.Transfers.list.init($('#date').val(),com );
        });
    });
</script>
@endsection
