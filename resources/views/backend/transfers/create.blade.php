@extends('backend.layouts.app')

@section('title', 'ادارة transfer | انشاء transfer')

@section('breadcrumb-links')
    @include('backend.transfers.includes.breadcrumb-links')
@endsection

@section('content')
	
    {{ Form::open(['route' => 'admin.transfers.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission', 'files' => true]) }}

    <div class="card">
        @include('backend.transfers.form')
        @include('backend.components.footer-buttons', ['cancelRoute' => 'admin.transfers.index'])
    </div><!--card-->
    {{ Form::close() }}
@endsection
