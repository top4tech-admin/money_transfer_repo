@extends('backend.layouts.app')

@section('title', app_name() . ' | تسليم حوالة')

@section('breadcrumb-links')
@include('backend.transfers.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card-body">
<div class="row">
    <div class="col-sm-5">
        <h4 class="card-title mb-0">
            تسليم حوالة
        </h4>
    </div>
    <!--col-->
</div>
<!--row-->

<hr>

<div class="row mt-4 mb-4">
    <div class="col-6">
        
		<div class="form-group row">
        {{ Form::label('client_id', 'اسم الزبون', ['class' => 'col-md-2 from-control-label required']) }}

        <div class="col-md-7">
			<input type="text" id="cleint_name" class='form-control' placeholder="ادخل اسم الزبون" autocomplete="off" ></input>
			<input type="text" id="cleint_id_input" name="client_id" value="" class='form-control' hidden></input>
        </div>
		<div class="col-md-3">
		<button type="button" id="btn-search-transfer" class="btn btn-danger hidden"> ابحث عن حولات</button>
		</div>
        <!--col-->
        </div>
        <!--form-group-->
		
		<!-- Start Get user by Ajax -->
		<div class="col-6 hidden" id="user-search-loader"><div class='ma-au loader'></div></div>
		<div class="col-12" id="user_search_box"></div>
		<!-- End Get user by Ajax -->
	
    </div>
    <!--col-->
	
	<div class="col-6">
		<!-- Start Get Transfers by Ajax -->
		<div class="col-6 hidden" id="transfers-search-loader"><div class='ma-au loader'></div></div>
		<div class="col-12" id="transfers_search_box"></div>
		<!-- End Get Transfers by Ajax -->
	</div>
<!--row-->
</div>


<!-- Modal -->
<div class="modal fade" id="confirm-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title h1" id="exampleModalLongTitle">تأكيد التسليم</p>
        <button type="button" class="close float-left" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body bac-color-gray">
        <p class="h2"><b>{{$logged_in_user->name}}, هل تريد تسليم الحوالة التالية بالفعل ؟</b></p>
		<hr/>
		<p class="h3"><b>الاسم:</b> <label id="transfr-name"></label></p>
		<p class="h3"><b>القيمة:</b> <label id="transfr-amount"></label></p>
		<p class="h3"><b>الشركة:</b> <label id="transfr-company"></label></p>
		<p class="h4"><b>ملاحظة:</b> <label id="transfr-note"></label></p>

      </div>
      <div class="modal-footer">
		  <div class="float-right alert alert-danger">
			ملاحظة: لا يمكن التراجع عن هذه الخطوة في حال الموافقة
		  </div>
       </div>
	   <div class="modal-footer">
		<form method="POST" accept-charset="UTF-8" class="form-horizontal" role="form" id="form-action" enctype="multipart/form-data">
		@csrf
		<input name="_method" type="hidden" value="PATCH">
		

		<div class="form-group row">
        {{ Form::label('birthday', 'تاريخ الميلاد', ['class' => 'col-12 col-md-3 from-control-label required']) }}

        <div class="col-md-5">
			<input type="date" id="birthday" name="birthday" required />
        </div>
		<div class="col-md-4">
		(سنة | شهر | يوم)
		</div>
        <!--col-->
        </div>
        <!--form-group-->
		
		<div class="form-group row">
        {{ Form::label('mother_name', 'اسم الام', ['class' => 'col-12 col-md-3 from-control-label required']) }}

        <div class="col-12 col-md-9">
            {{ Form::text('mother_name', null, ['class' => 'form-control', 'placeholder' => 'اسم الام', 'required' => 'required']) }}
        </div>
        <!--col-->
        </div>
        <!--form-group-->
		<hr/>
		
		
		<input name='status' type='text' value='T' hidden></input>
		<input id="transfer-id-input" name='id' type='text' value='' hidden></input>
		<button type="button" class="btn btn-secondary size-20" data-dismiss="modal">تراجع</button>
        <button type="submit" id="submitForm" class="btn btn-primary size-20">نعم تأكيد</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!--card-body-->
@endsection

