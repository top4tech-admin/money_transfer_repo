@extends('backend.layouts.app')

@section('title', 'transfers management | تعديل transfer'))

@section('breadcrumb-links')
    @include('backend.transfers.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::model($transfer, ['route' => ['admin.transfers.update', $transfer], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role', 'files' => true]) }}

    <div class="card">
        @include('backend.transfers.form')
        @include('backend.components.footer-buttons', [ 'cancelRoute' => 'admin.transfers.index', 'id' => $transfer->id ])
    </div><!--card-->
    {{ Form::close() }}
@endsection
