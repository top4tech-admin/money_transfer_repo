@extends('backend.layouts.app')

@section('title', app_name() . ' | ادراة الحوالات')

@section('breadcrumb-links')
@include('backend.transfers.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row justify-content-between">
            <div class="col-sm-2">
                <h4 class="card-title mb-0">
                حوالات الشركة
                </h4>
            </div>
			<div class="col-sm-4">
                <h6 class="card-title mb-0">
                    عدد الحوالات المحددة:   <span id="span-trans-number"></span>   
                </h6>
            </div>
			<div class="col-sm-4">
                <h6 class="card-title mb-0">
                    قيمة الحوالات المحددة: <span id="span-trans-amount"></span> 
                </h6>
            </div>
        </div>
        <!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="transfers-table" class="table" data-ajax_url="{{ route('admin.transfers.company.get',['com_id'=>$com_id]) }}">
                        <thead>
                            <tr>
                                <th>الشركة</th> 
								<th>الزبون</th> 
								<th>القيمة</th> 
								<th>الحالة</th> 
								<th>اسم الام</th> 
								<th>موبايل</th> 
								<th>الميلاد</th>
                                <th>{{ trans('labels.backend.access.pages.table.createdat') }}</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Transfers.list.init();
    });
</script>
@endsection
