@extends('backend.layouts.app')

@section('title', app_name() . ' | ادراة الحوالات')

@section('breadcrumb-links')
@include('backend.transfers.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">

		<div class="row justify-content-between">
            <div class="col-sm-2">
                <h4 class="card-title mb-0">
                    الحوالات المسلمة 
                </h4>
            </div>
			<div class="col-sm-3">
                <h6 class="card-title mb-0">
                    عدد الحوالات المحددة:   <span id="span-trans-number"></span>   
                </h6>
            </div>
			<div class="col-sm-3">
                <h6 class="card-title mb-0">
                   قيمة الحوالات المحددة: <span id="span-trans-amount"></span> 
                </h6>
            </div>
			<div class="col-sm-2">
                <h4 class="card-title mb-0">
                    <a href="{{ route('admin.transfers.create') }}" class="btn btn-success">انشاء حوالة</a> 
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->
		<hr/>
		<div class="row alert alert-success">

			<div class="col-sm-6">
                <div class="form-check form-check-inline">
					<label class="form-check-label" for="date"> فلترة حسب تاريخ التسليم</label>
				    <input type="date" id="date" />
				</div>
            </div>
            
            <div class="col-sm-5">
                <div class="form-check form-check-inline">
					<label class="form-check-label" for="date"> حدد  الشركات </label>
				    <select id="com" multiple>
                        <option disabled selected>جميع الشركات</option>
                        @foreach($companies as $company)
                            <option value="{{$company->id}}">{{$company->name}}</option>
                        @endforeach
                    </select>
                    <button id="selectCompanies" >OK</button>
				</div>
            </div>
        </div>
        <!--row-->
		
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="transfers-table" class="table display" data-ajax_url="{{ route("admin.transfers.recived.get") }}">
                        <thead>
                            <tr>
								<th>الشركة</th> 
								<th>الزبون</th> 
								<th>القيمة</th> 
								<th>الحالة</th> 
								<th>اسم الام</th> 
								<th>موبايل</th> 
								<th>الميلاد</th>
                                <th>{{ trans('labels.backend.access.pages.table.createdat') }}</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->



    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {

		FTX.Transfers.list.init();
		/*
		// checkbox for today transfers
		var today = new Date().toISOString().substring(0,10).replace("T"," ");
		$('#today').on('change',function(e){
			$("#transfers-table").dataTable().fnDestroy();
			if(e.currentTarget.checked){
			FTX.Transfers.list.init(today);
			}
			else{
				 FTX.Transfers.list.init();
			}
		})
		*/
		
		$('#date').on('change',function(e){
            let com = $('#com').val();
            console.log(com);
			$("#transfers-table").dataTable().fnDestroy();
			FTX.Transfers.list.init($(this).val(),com );
		});

        $('#selectCompanies').on('click',function(e){
            let com = $('#com').val();
            console.log(com);
            $("#transfers-table").dataTable().fnDestroy();
            FTX.Transfers.list.init($('#date').val(),com );
        });

    });
</script>
@endsection
