<div class="col-12">
<div class="table-responsive">
    <table class="table table-hover">
	
	@if($transfer->status == 'T')
        <tr>
            <th>@lang('labels.backend.access.users.tabs.content.overview.status')</th>
			<td>
				<span class='badge badge-success'>الحوالة مسلمة</span>
			</td>
        </tr>
		<tr>
            <th>تاريخ تسليم الحوالة</th>
			<td>
				<span>{{$transfer->delivered_at}}</span>
			</td>
        </tr>
	@else
		<tr>
            <th>@lang('labels.backend.access.users.tabs.content.overview.status')</th>
			<td>
				<span class='badge badge-danger'>الحوالة غير مسلمة</span>
			</td>
        </tr>
	@endif
        <tr>
            <th class="col-3">الشركة</th>
            <td class="col-10">{{ $transfer->company->name }}</td>
        </tr>

		<tr>
            <th>الزبون</th>
            <td>{{ $transfer->client->name }}</td>
        </tr>
		
		<tr>
            <th>قيمة الحوالة</th>
            <td>{{ $transfer->amount }}</td>
        </tr>
			
		<tr>
            <th>انشاء الحوالة</th>
            <td>{{$transfer->creator->first_name}} {{$transfer->creator->last_name}}</td>
        </tr>
		
		<tr>
            <th>سلم الحوالة</th>
            <td>{{ $transfer->deliver->first_name}} {{$transfer->deliver->last_name}}</td>
        </tr>
		
		<tr>
            <th>تاريخ الانشاء</th>
            <td>{{$transfer->created_at}}</td>
        </tr>
		
        <tr>
            <th>تاريخ اخر تعديل</th>
            <td>{{ $transfer->updated_at }}</td>
        </tr>

        <tr>
            <th>ملاحظات على الحوالة</th>
            <td>{{$transfer->note}}</td>
        </tr>

    </table>
</div>
</div><!--table-responsive-->
