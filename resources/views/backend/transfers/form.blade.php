<div class="card-body">
<div class="row">
    <div class="col-sm-5">
        <h4 class="card-title mb-0">
            ادارت الحوالات
            <small class="text-muted">{{ (isset($transfer)) ? 'تعديل انشاء' : 'انشاء  حوالة' }}</small>
        </h4>
    </div>
    <!--col-->
</div>
<!--row-->

<hr>

<div class="row mt-4 mb-4">
    <div class="col-6">
        
		<div class="form-group row">
        {{ Form::label('company_id', 'اسم الشركة', ['class' => 'col-md-2 from-control-label required']) }}

        <div class="col-md-10">
		@if(empty($companies))
			<div class="loader"></div>
		@else
		<select name="company_id" class='form-control' required>
		<option value="" disabled selected>اختر شركة</option>
		@foreach($companies as $com)
			
			@if(isset($transfer))
			<option value="{{$com->id}}" {{($transfer->company_id == $com->id) ? 'selected' : ''}}>{{$com->name}}</option>
			@elseif(session()->get('company_id'))
			<option value="{{$com->id}}" {{(session()->get('company_id') ==  $com->id) ? 'selected': ''}}>{{$com->name}}</option>
			@else
			<option value="{{$com->id}}">{{$com->name}}</option>	
			@endif
			
		@endforeach
		</select>
		@endif

        </div>
        <!--col-->
        </div>
        <!--form-group-->
	
		<div class="form-group row">
        {{ Form::label('client_id', 'اسم الزبون', ['class' => 'col-md-2 from-control-label required']) }}

        <div class="col-md-10">
		@if(!isset($transfer))
            <input type="text" id="cleint_name" name="client-name" class='form-control' placeholder="ادخل اسم الزبون" autocomplete="off" required></input>
			<input type="text" id="cleint_id_input" name="client_id" value="" class='form-control'  hidden></input>
		@else
			<input type="text" name="client_id" value="{{$transfer->client_id}}" class='form-control' hidden disabled></input>
			{{$transfer->client->name}}
		@endif
        </div>
        <!--col-->
        </div>
        <!--form-group-->

		<div class="form-group row">
        {{ Form::label('phone', ' رقم الموبايل', ['class' => 'col-md-2 from-control-label required']) }}

        <div class="col-md-10">
		@if(!isset($transfer))
			<input type="text" name="client-phone" id="phone-input" class='form-control' placeholder="ادخل رقم الموبايل" autocomplete="off" ></input>
		@else
			{{$transfer->client->phone}}
		@endif
        </div>
        <!--col-->
        </div>
        <!--form-group-->
		
		<div class="form-group row">
        {{ Form::label('amount', 'القيمة', ['class' => 'col-md-2 from-control-label required']) }}

        <div class="col-md-10">
		@if(isset($transfer))
            {{ Form::number('amount', null, ['class' => 'form-control', 'required' => 'required', 'min' => 0]) }}
			<input type="number"  name="old_amount" value="{{$transfer->amount}}" hidden />
		@else
			{{ Form::number('amount', null, ['class' => 'form-control', 'placeholder' => 'ادخل القيمة','required' => 'required', 'min' => 0]) }}
		@endif
			
        </div>
        <!--col-->
        </div>
        <!--form-group-->


            <div class="form-group row">
                {{ Form::label('note', 'ملاحظة', ['class' => 'col-md-2 from-control-label']) }}

                <div class="col-md-10">
                    {{ Form::textarea('note', null, ['class' => 'form-control', 'placeholder' => 'اكتب ملاحظة']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->
			@if(isset($transfer))
			<div class="form-group row hidden">
            {{ Form::label('status', 'حالة الحوالة', ['class' => 'col-md-2 from-control-label required']) }}

            <div class="col-md-10">
				<div class="col-md-10">
				<input type="radio"  name="status" value="T" id="t" {{$transfer->status == 'T' ? 'checked' : ''}}/>
				<label class="form-check-label" for="T">تم التسليم</label>
				</div>
				<div class="col-md-10">
				<input type="radio" name="status" value="F" id="f" {{$transfer->status == 'F' ? 'checked' : ''}} />
				<label class="form-check-label" for="F">لم يتم التسليم</label>
				</div>
            </div>
            <!--col-->
            </div>
            <!--form-group-->
			@else
			<input type="radio"  name="status" value="F" id="f" checked hidden />
			@endif
    </div>
    <!--col-->
	
	<!-- Start Get user by Ajax -->
	<div class="col-6 hidden" id="user-search-loader"><div class='ma-au loader'></div></div>
	<div class="col-6" id="user_search_box"></div>
	<!-- End Get user by Ajax -->
	
</div>
<!--row-->
</div>
<!--card-body-->

@section('pagescript')
<script type="text/javascript">
FTX.Utils.documentReady(function() {
    FTX.Pages.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
});
</script>
@stop
