<div class="card-body">
<div class="row">
    <div class="col-sm-5">
        <h4 class="card-title mb-0">
            ادارة الزبائن
            <small class="text-muted">{{ (isset($client)) ? 'تعديل بيانات الزبون' : 'انشاء بيانات الزبون' }}</small>
        </h4>
    </div>
    <!--col-->
</div>
<!--row-->

<hr>

<div class="row mt-4 mb-4">
    <div class="col-12 col-lg-6">
        
		<div class="form-group row">
        {{ Form::label('name', 'الاسم', ['class' => 'col-12 col-md-2 from-control-label required']) }}

        <div class="col-md-10">
            {{ Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'الاسم']) }}
        </div>
        <!--col-->
        </div>
        <!--form-group-->

		<div class="form-group row hidden">
        {{ Form::label('identification', 'الهوية', ['class' => 'col-12 col-md-2 from-control-label required']) }}

        <div class="col-md-10">
			{{ Form::text('identification', null, ['class' => 'form-control', 'placeholder' => 'الهوية']) }}
        </div>
        <!--col-->
        </div>
        <!--form-group-->

		<div class="form-group row hidden">
        {{ Form::label('finger_print', 'البصمة', ['class' => 'col-12 col-md-2 from-control-label required']) }}

        <div class="col-md-10">
			{{ Form::text('finger_print', null, ['class' => 'form-control', 'placeholder' => 'البصمة' ]) }}
        </div>
        <!--col-->
        </div>
        <!--form-group-->
		
		<div class="form-group row">
        {{ Form::label('mother_name', 'اسم الام', ['class' => 'col-12 col-md-2 from-control-label required']) }}

        <div class="col-12 col-md-10">
            {{ Form::text('mother_name', null, ['class' => 'form-control', 'placeholder' => 'اسم الام']) }}
        </div>
        <!--col-->
        </div>
        <!--form-group-->

		<div class="form-group row">
        {{ Form::label('birthday', 'تاريخ الميلاد', ['class' => 'col-12 col-md-2 from-control-label required']) }}

        <div class="col-md-6">
		@if(isset($client))
			<input type="date"  name="birthday" value="{{$client->birthday}}" \>
		@else
			<input type="date"  name="birthday" \>
		@endif
        </div>
		<div class="col-md-4">
		(سنة | شهر | يوم)
		</div>
        <!--col-->
        </div>
        <!--form-group-->

<div class="form-group row">
        {{ Form::label('phone', 'موبايل', ['class' => 'col-12 col-md-2 from-control-label required']) }}

        <div class="col-md-10">
            {{ Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'موبايل']) }}
        </div>
        <!--col-->
        </div>
        <!--form-group-->

    </div>
    <!--col-->
</div>
<!--row-->
</div>
<!--card-body-->

  
@section('pagescript')
<script type="text/javascript">
FTX.Utils.documentReady(function() {
    FTX.Clients.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
});
</script>
@stop
