@extends('backend.layouts.app')

@section('title', 'ادارة client | انشاء client')

@section('breadcrumb-links')
    @include('backend.clients.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.clients.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission', 'files' => true]) }}

    <div class="card">
        @include('backend.clients.form')
        @include('backend.components.footer-buttons', ['cancelRoute' => 'admin.clients.index'])
    </div><!--card-->
    {{ Form::close() }}
@endsection
