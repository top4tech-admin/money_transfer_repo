@extends('backend.layouts.app')

@section('title', 'clients management | تعديل client'))

@section('breadcrumb-links')
    @include('backend.clients.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::model($client, ['route' => ['admin.clients.update', $client], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role', 'files' => true]) }}

    <div class="card">
        @include('backend.clients.form')
        @include('backend.components.footer-buttons', [ 'cancelRoute' => 'admin.clients.index', 'id' => $client->id ])
    </div><!--card-->
    {{ Form::close() }}
@endsection
