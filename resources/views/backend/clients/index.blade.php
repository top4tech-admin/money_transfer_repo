@extends('backend.layouts.app')

@section('title', app_name() . ' | ادراة clients')

@section('breadcrumb-links')
@include('backend.clients.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">

		<div class="row justify-content-between">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    ادراة الزبائن 
                </h4>
            </div>
			<div class="col-sm-2">
                <h4 class="card-title mb-0">
                    <a href="{{ route('admin.clients.create') }}" class="btn btn-success">انشاء حساب لزبون جديد</a> 
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->
		
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="clients-table" class="display nowrap" data-ajax_url="{{ route("admin.clients.get") }}">
                        <thead>
                            <tr>
                                <th>الاسم</th> 
								<th>الام</th> 
								<th>الميلاد</th> 
								<th>موبايل</th> 
                                <th>{{ trans('labels.backend.access.pages.table.createdat') }}</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Clients.list.init();
    });
</script>
@endsection
