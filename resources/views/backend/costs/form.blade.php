<div class="card-body">
<div class="row">
    <div class="col-sm-5">
        <h4 class="card-title mb-0">
            ادارة المصاريف
            <small class="text-muted">{{ (isset($cost)) ? 'تعديل المصروف' : 'انشاء مصروف جديد' }}</small>
        </h4>
    </div>
    <!--col-->
</div>
<!--row-->

<hr>

<div class="row mt-4 mb-4">
    <div class="col-6">
        
<div class="form-group row">
        {{ Form::label('note', 'الوصف', ['class' => 'col-md-2 from-control-label required']) }}

        <div class="col-md-10">
            {{ Form::text('note', null, ['class' => 'form-control', 'placeholder' => 'الوصف', 'required' => 'required']) }}
        </div>
        <!--col-->
        </div>
        <!--form-group-->

<div class="form-group row">
        {{ Form::label('type', 'النوع', ['class' => 'col-md-2 from-control-label required']) }}

        <div class="col-md-10">
			<select class="select form-control" name="type" required>
				<option value="" disabled selected>اختر نوع المصروف</option>
				@if(isset($cost))
				<option value="A" {{($cost->type == 'A') ? 'selected' : ''}}>A</option>
				<option value="B" {{($cost->type == 'B') ? 'selected' : ''}}>B</option>
				<option value="C" {{($cost->type == 'C') ? 'selected' : ''}}>C</option>
				@else
				<option value="A" >A</option>
				<option value="B" >B</option>
				<option value="C" >C</option>
				@endif
			</select>
        </div>
        <!--col-->
        </div>
        <!--form-group-->

<div class="form-group row">
        {{ Form::label('amount', 'القيمة', ['class' => 'col-md-2 from-control-label required']) }}

        <div class="col-md-10">
		@if(isset($cost))
            {{ Form::number('amount', null, ['class' => 'form-control', 'القيمة' => 'amount', 'min' => '0','required' => 'required']) }}
			<input type="number" name="old_amount" value="{{$cost->amount}}" hidden />
		@else
			{{ Form::number('amount', null, ['class' => 'form-control', 'القيمة' => 'amount', 'min' => '0','required' => 'required']) }}
		@endif
        </div>
        <!--col-->
        </div>
        <!--form-group-->

    </div>
    <!--col-->
</div>
<!--row-->
</div>
<!--card-body-->

@section('pagescript')
<script type="text/javascript">
FTX.Utils.documentReady(function() {
    FTX.Pages.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
});
</script>
@stop
