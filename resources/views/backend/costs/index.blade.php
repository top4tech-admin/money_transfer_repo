@extends('backend.layouts.app')

@section('title', app_name() . ' | ادراة costs')

@section('breadcrumb-links')
@include('backend.costs.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">

		<div class="row justify-content-between">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    ادراة المصاريف 
                </h4>
            </div>
			<div class="col-sm-2">
                <h4 class="card-title mb-0">
                    <a href="{{ route('admin.costs.create') }}" class="btn btn-success">انشاء مصروف جديد</a> 
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="costs-table" class="table" data-ajax_url="{{ route("admin.costs.get") }}">
                        <thead>
                            <tr>
                                
					<th>الوصف</th> 
					<th>النوع</th> 
					<th>القيمة</th> 
                                <th>{{ trans('labels.backend.access.pages.table.createdat') }}</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Costs.list.init();
    });
</script>
@endsection
