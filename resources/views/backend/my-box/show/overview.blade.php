<div class="col">
<div class="table-responsive">
    <table class="table table-hover">

        <tr>
            <th>راس المال</th>
            <td>{{ $my_box->my_asset }}</td>
        </tr>

        <tr>
            <th>الموجودات</th>
            <td>{{ $my_box->all_asset }}</td>
        </tr>

        <tr>
            <th>المطاليب</th>
            <td>{{$my_box->all_required}}</td>
        </tr>
		
		<tr>
            <th>الميزان</th>
            <td>{{$my_box->all_asset - $my_box->all_required}}</td>
        </tr>

    </table>
</div>
</div><!--table-responsive-->
