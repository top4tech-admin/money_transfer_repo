@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>@lang('strings.backend.dashboard.welcome') {{ $logged_in_user->name }}!</strong>
                </div><!--card-header-->
				
                <div class="card-body">
                    <div class="col-5 alert {{($mybox->all_asset - $mybox->all_required == 0 ? 'alert-info' : ($mybox->all_asset - $mybox->all_required) > 0 ) ? 'alert-success' : 'alert-danger'}}">
					<div>
						<label class="h1">الصندوق</label>
					</div>
					<hr/>
					
					<div>
						<label class="h2">الموجودات الاجمالية: {{$mybox->all_asset}}</label>
					</div>
					<div>
						<label class="h2">المطاليب: {{$mybox->all_required}}</label>
					</div>
					<div>
						<label class="h2">راس المال: {{$mybox->my_asset}}</label>
					</div>
					</div>
                </div><!--card-body-->
				
				
				<hr/>
				
				<div class="card-body">
				<label class="h1"> الشركات</label>
				</div>
				<div class="row justify-content-around">
				@foreach($companies as $company)
				<a class="col-5" href="{{route('admin.transfers.company',['com_id'=>$company->id])}}">
				
                    <div class="info-box alert {{($company->asset - $company->required == 0 ? 'alert-info' : ($company->asset - $company->required) > 0 ) ? 'alert-success' : 'alert-danger'}}">
					<div>
						<label class="h1">{{$company->name}}</label>
					
					</div>
					<hr/>
					<div>
						<label class="h2">الرصيد: {{$company->asset}}</label>
					</div>
					<div>
						<label class="h2">حوالات يجب تسليمها: {{$company->required}}</label>
					</div>
					</div>
				</a>
				@endforeach
				</div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection
